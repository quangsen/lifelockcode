<?php if(is_singular()): ?>
	<div class="post-thumbnail">
		<?php echo the_post_thumbnail(); ?>

	</div>
<?php else: ?>
	<a class="post-thumbnail" href="<?php the_permalink();?>" aria-hidden="true">
		<?php echo the_post_thumbnail('post-thumbnail', array('alt' => get_the_title())); ?>

	</a>
<?php endif; ?>