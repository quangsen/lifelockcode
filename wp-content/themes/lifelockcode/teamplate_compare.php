<?php
/*
Template Name: Compare
 */
get_header();?>

    <?php while (have_posts()): the_post();?>
	        <?php
	$id_page = get_the_ID();
	$id_code = '134';
	$get_post_code = new WP_Query(array('post_type' => 'Code', 'posts_per_page' => 1));
	while ($get_post_code->have_posts()): $get_post_code->the_post();
		$id_code = get_the_ID();
	endwhile;

    $arr_infoproduct = [];
    $info_products = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1,) ); 
    while($info_products->have_posts() ) : 
        $info_products->the_post();
        $arr_infoproduct[get_the_title()]['name'] = title_case(get_the_title());
        $arr_infoproduct[get_the_title()]['price_month'] = get_field('price_month', get_the_ID());
        $arr_infoproduct[get_the_title()]['price_annual'] = get_field('price_annual', get_the_ID());
        $arr_infoproduct[get_the_title()]['id'] = get_the_ID();
    endwhile;

	$max_discount = get_post_meta($id_code, 'discount', true);
	$max_term = get_post_meta($id_code, 'term', true);
	?>
    <div class="innerCont w1354">
    <div id="kenproducts" style="color: black;">
        <div class="largeTitle">
            <h1><?php echo get_the_title($id_page);?></h1>
        </div>
        <div class="box-shadow pagecompare">
            <h2 class="title-main">LifeLock vs The Competition</h2>
            <p class="description-main" style="text-align: center;padding-bottom: 30px;">(Last Updated December 2015)</p>
            <div class="col-md-3 col-xs-12">
        <div class="fr shield shield-js">
    	    <h3 style="text-align: center;"><strong><span style="font-size: 12pt;">THE #1 LIFELOCK PROMO CODE:</span></strong></h3>
    	    <div id="cl-effect-5" class="cl-effect-5">
    	    <h2 style="text-align: center;"><a href="<?php echo render_url($id_code, $id_page); ?>"title="LLC3015"><span style="color: #ffff66;" data-hover="LLC3015"><?php the_field('code', $id_code);?></span></a></h2>
    	    </div>
	       <p style="text-align: center;text-align: center;margin-top: 15px;padding: 0 22px;"><strong><span style="font-size: 18pt;"><strong style="text-align: center;"><?php echo $max_discount; ?>% Off + <?php echo $max_term; ?> Days Risk Free*</p>
	    </div>
	    </div>
        <div class="col-md-9 col-xs-12">
            <table>
                <tr class="tr1">
                    <td><div class="logo-lifelock-text">
                            <div class="logo"><img src="<?php bloginfo('template_directory');?>/image/logo-icon-text.gif" alt="Life Lock promo code" width="37" height="36"></div>
                            <div class="text">
                            <p>Life<span>Lock</span></p>
                            </div>
                            </div></td>
                    <td><img src="<?php bloginfo('template_directory');?>/image/Compare-identity-guard.png"/></td>
                    <td><img src="<?php bloginfo('template_directory');?>/image/Compare-identity_force_logo.gif"/></td>
                </tr>
                <tr class="tr2">
                <td><p>LifeLock</p></td>
                <td><p>Identity Guard</p></td>
                <td><p>Identity Force</p></td>
                </tr>
                  <tr>
                <td><p><?php echo $arr_infoproduct['STANDARD']['name'].' - $'.$arr_infoproduct['STANDARD']['price_month'].'/month'; ?> </p></td>
                <td><p>Gold – $9.99/month   </p></td>
                <td><p></p></td>
                </tr>
                  <tr>
                <td><p><?php echo $arr_infoproduct['ADVANTAGE']['name'].' - $'.$arr_infoproduct['ADVANTAGE']['price_month'].'/month'; ?> </p></td>
                <td><p>Total Protection $19.99/month</p></td>
                <td><p>UltraSecure $17.95/month</p></td>
                </tr>
                  <tr>
                <td><p><?php echo $arr_infoproduct['ULTIMATE PLUS']['name'].' - $'.$arr_infoproduct['ULTIMATE PLUS']['price_month'].'/month'; ?> </p></td>
                <td><p>Platinum $24.99/month</p></td>
                <td><p>UltraSecure + Credit $23.95/month</p></td>
                </tr>
                <tr>
                <td class="td1"><p>LIFELOCKCODES PROMO CODE</p>
                    <p style="color: #ffff66;"><a style="color: #ffff66;" href="<?php echo render_url($id_code); ?>"><?php the_field('code', $id_code);?></a> - <?php echo $max_discount; ?>% Off + <?php echo $max_term; ?> Days Risk Free*</p>
                </td>
                <td><p></p></td>
                <td><p></p></td>
                </tr>
            </table>
            </div>
            <p class="clear" style="clear: both;"></p>
            <div class="col-md-12 table-2">
            <p style="border: none; color: #ddd222; display: block; padding-bottom: 8px; margin-left:; margin-top: 10px; font-weight: bold; text-align: center;">
	            <span>COMPARISON OF PREMIUM PLANS</span>
	        </p>
            <table>
                <tbody>
                <tr>

	    <tr class="box-logo" style="border-color: #590303; border-width: 1px;"></tr>
	    <tr class="title-table-child">
	    <td class="xl9319319" style="height: 18.0pt; border-top: none; border-bottom: none;" height="24"><span></span></td>
	    <td class="xl8519319" style="background-color: #dd2225; color: white; border-top: none; font-size: 16px; text-align: center;"><span>Lifelock</span></td>
	    <td class="xl7919319" style="background-color: #dd2225; color: white; border-top: none; border-left: none; font-size: 16px; text-align: center;"><span>Identity Guard&nbsp;</span></td>
	    <td class="xl7919319" style="background-color: #dd2225; color: white; border-top: none; border-left: none; font-size: 16px; text-align: center;"><span>Identity Force</span></td>
	    </tr>
	    <tr>
	    <td class="xl9319319" style="font-weight: bold; line-height: 1; padding-top: 0px; height: 18.0pt; border-top: none;" height="24">MONITORING</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Address Change Verification </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Fictitious Identity Monitoring </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> File Sharing Network Searches </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Black Market Website Surveillance </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Court Records Scanning </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Sex Offender Registry Reports </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr>
	    <td class="xl9319319" style="font-weight: bold; line-height: 1; padding-top: 30px; height: 18.0pt; border-top: none;" height="24">ALERTS</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Automatic Fraud Alerts </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Credit Card Activity Alerts† </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Credit Inquiry Alerts† </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Bank Account Takeover Alerts† </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Checking and Savings Account Activity Alerts† </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Checking and Savings Account Application Alerts† </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Data Breach Notifications </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Investment Account Activity Alerts† </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    </tr>
	    <tr>
	    <td class="xl9319319" style="font-weight: bold; line-height: 1; padding-top: 30px; height: 18.0pt; border-top: none;" height="24">CREDIT</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Online Annual Tri-Bureau Credit Reports </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Online Annual Credit Report </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Monthly Credit Score Tracking </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Online Annual Credit Score </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr>
	    <td class="xl9319319" style="font-weight: bold; line-height: 1; padding-top: 30px; height: 18.0pt; border-top: none;" height="24">SUPPORT</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    <td class="xl8519319" style="border-top: none;">&nbsp;</td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Identity Restoration Support </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Lost Wallet Protection </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Reduced Public Exposure Tool </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> $1 Million Total Service Guarantee‡ </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr class="tick-check">
	    <td class="xl9519319" style="height: 18.0pt; border-top: none;" height="24"><span> Reduced Pre-Approved Credit Card Offers </span></td>
	    <td class="check-center"><span class="tick-true">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-close">&nbsp;</span></td>
	    <td class="check-center" style="border-left: none;"><span class="tick-true">&nbsp;</span></td>
	    </tr>
	    <tr>
	    <td class="xl9119319" style="height: 18.0pt; text-align: right;" height="24">&nbsp;</td>
	    <td class="xl8319319" style="height: 18.0pt; text-align: center; color: #ddd222; background-color: #4f4d4c; line-height: 1.8;" height="24"><a href="<?php echo render_url($id_code, $id_page); ?>" style="color: #dccf00; font-size: 15px; text-decoration: underline;" title="ENROLL NOW">ENROLL NOW</a><br>Save <?php echo $max_discount; ?>% Off + <?php echo $max_term; ?> Days Risk Free*</td>
	    <td class="xl8019319" style="border-left: none;">&nbsp;</td>
	    <td class="xl8019319" style="border-left: none;">&nbsp;</td>
	    </tr>
	    <tr style="display: none;">
	    <td style="width: 185pt;" width="247">&nbsp;</td>
	    <td style="width: 195pt;" width="260">&nbsp;</td>
	    <td style="width: 131pt;" width="175">&nbsp;</td>
	    <td style="width: 169pt;" width="225">&nbsp;</td>
	    </tr>
	    </tbody>
	</table>
	    </div>
    	    <p class="note-table" style="text-align: center;"><em>Chart Data last updated December, 2015.</em><em></em></p>
    	    <p class="content-bottom" style="text-align: justify;">Although efforts are made to ensure the accuracy of the information, no guarantees are made as to its correctness. The information contained on this site is for informational purposes only, and may not apply to your situation. The author, publisher, distributor, provider or any company whose data is included provides no warranty about the content or accuracy of content enclosed. Neither the author, publisher, distributor, provider or any company whose data is included shall be liable for any loss of profit or any other damages resulting from the use of this guide.</p>
    	    <p class="link-mail content-bottom" style="text-align: justify;">If you are aware of any errors or any updated data please notify us at <span id="cloak68307"><a href="mailto:info@lifelockcodes.com" title="Social Twitter">info@lifelockcodes.com</a></span>. Please verify the accuracy of information before using to make any decisions.</p>
	    </div>

	    </div>
	        <?php endwhile; // end of the loop. ?>

    </div><!-- #content -->
</div><!-- #primary -->

 <?php get_footer()?>