<?php
use Symfony\Component\DomCrawler\Crawler;

require __DIR__ . '/bootstrap/app.php';
require __DIR__ . '/vendor/autoload.php';
/**
 * lifelockcode functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since lifelockcode 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since lifelockcode 1.0
 */
if (!isset($content_width)) {
	$content_width = 660;
}

/**
 * lifelockcode only works in WordPress 4.1 or later.
 */
if (version_compare($GLOBALS['wp_version'], '4.1-alpha', '<')) {
	require get_template_directory() . '/inc/back-compat.php';
}

if (!function_exists('lifelockcode_setup')):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since lifelockcode 1.0
 */
	function lifelockcode_setup() {

		/*
			 * Make theme available for translation.
			 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/lifelockcode
			 * If you're building a theme based on lifelockcode, use a find and replace
			 * to change 'lifelockcode' to the name of your theme in all the template files
		*/
		load_theme_textdomain('lifelockcode');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
		*/
		add_theme_support('title-tag');

		/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size(825, 510, true);

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(array(
			'primary-menu' => __('Primary Menu', 'lifelockcode'),
			'social' => __('Social Links Menu', 'lifelockcode'),
		));

		/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
		*/
		add_theme_support('html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		));

		/*
			 * Enable support for Post Formats.
			 *
			 * See: https://codex.wordpress.org/Post_Formats
		*/
		add_theme_support('post-formats', array(
			'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat',
		));

		/*
			 * Enable support for custom logo.
			 *
			 * @since lifelockcode 1.5
		*/
		add_theme_support('custom-logo', array(
			'height' => 248,
			'width' => 248,
			'flex-height' => true,
		));

		$default_color = trim($color_scheme[0], '#');

		// Setup the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('lifelockcode_custom_background_args', array(
			'default-color' => $default_color,
			'default-attachment' => 'fixed',
		)));

		/*
			 * This theme styles the visual editor to resemble the theme style,
			 * specifically font, colors, icons, and column width.
		*/
		add_editor_style(array('css/editor-style.css', 'genericons/genericons.css', lifelockcode_fonts_url()));

		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support('customize-selective-refresh-widgets');
	}
endif; // lifelockcode_setup
add_action('after_setup_theme', 'lifelockcode_setup');

if (!function_exists('lifelockcode_fonts_url')):
/**
 * Register Google fonts for lifelockcode.
 *
 * @since lifelockcode 1.0
 *
 * @return string Google fonts URL for the theme.
 */
	function lifelockcode_fonts_url() {
		$fonts_url = '';
		$fonts = array();
		$subsets = 'latin,latin-ext';

		/*
			 * Translators: If there are characters in your language that are not supported
			 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
		*/
		if ('off' !== _x('on', 'Noto Sans font: on or off', 'lifelockcode')) {
			$fonts[] = 'Noto Sans:400italic,700italic,400,700';
		}

		/*
			 * Translators: If there are characters in your language that are not supported
			 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
		*/
		if ('off' !== _x('on', 'Noto Serif font: on or off', 'lifelockcode')) {
			$fonts[] = 'Noto Serif:400italic,700italic,400,700';
		}

		/*
			 * Translators: If there are characters in your language that are not supported
			 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
		*/
		if ('off' !== _x('on', 'Inconsolata font: on or off', 'lifelockcode')) {
			$fonts[] = 'Inconsolata:400,700';
		}

		/*
			 * Translators: To add an additional character subset specific to your language,
			 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
		*/
		$subset = _x('no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'lifelockcode');

		if ('cyrillic' == $subset) {
			$subsets .= ',cyrillic,cyrillic-ext';
		} elseif ('greek' == $subset) {
		$subsets .= ',greek,greek-ext';
	} elseif ('devanagari' == $subset) {
		$subsets .= ',devanagari';
	} elseif ('vietnamese' == $subset) {
		$subsets .= ',vietnamese';
	}

	if ($fonts) {
		$fonts_url = add_query_arg(array(
			'family' => urlencode(implode('|', $fonts)),
			'subset' => urlencode($subsets),
		), 'https://fonts.googleapis.com/css');
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since lifelockcode 1.1
 */
function lifelockcode_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action('wp_head', 'lifelockcode_javascript_detection', 0);

/**
 * Enqueue scripts and styles.
 *
 * @since lifelockcode 1.0
 */
function lifelockcode_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style('lifelockcode-fonts', lifelockcode_fonts_url(), array(), null);

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style('genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2');

	// Load our main stylesheet.
	wp_enqueue_style('lifelockcode-style', get_stylesheet_uri());

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style('lifelockcode-ie', get_template_directory_uri() . '/css/ie.css', array('lifelockcode-style'), '20141010');
	wp_style_add_data('lifelockcode-ie', 'conditional', 'lt IE 9');

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style('lifelockcode-ie7', get_template_directory_uri() . '/css/ie7.css', array('lifelockcode-style'), '20141010');
	wp_style_add_data('lifelockcode-ie7', 'conditional', 'lt IE 8');

	wp_enqueue_script('lifelockcode-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true);
}

class App {
	public function __construct() {
		add_action('init', [$this, 'custom_rewrite_rule']);
		add_action('after_setup_theme', [$this, 'lifelockcode_setup']);
		add_action('wp_enqueue_scripts', [$this, 'style']);
		add_action('widgets_init', [$this, 'lifelockcode_widgets_init']);
	}
	public function lifelockcode_setup() {
		register_nav_menus(array(
			'primary-menu' => __('Primary Menu', 'lifelockcode'),
			'social' => __('Social Links Menu', 'lifelockcode'),
		));
	}
	public function style() {
		wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', false);
	}
	/**
	 * Register widget area.
	 *
	 * @since lifelockcode 1.0
	 *
	 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
	 */
	public function lifelockcode_widgets_init() {
		register_sidebar(array(
			'name' => __('Widget Area', 'lifelockcode'),
			'id' => 'sidebar-1',
			'description' => __('Add widgets here to appear in your sidebar.', 'lifelockcode'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		));

		register_sidebar(array(
			'name' => __('button-top', 'lifelockcode'),
			'id' => 'button-top',
			'description' => __('Add button here to appear in top header.', 'lifelockcode'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		));
		register_sidebar(array(
			'name' => __('footer-1', 'lifelockcode'),
			'id' => 'footer-1',
			'description' => __('Add footer-1 here to appear in footer.', 'lifelockcode'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		));
		register_sidebar(array(
			'name' => __('footer-2', 'lifelockcode'),
			'id' => 'footer-2',
			'description' => __('Add footer-2 here to appear in footer.', 'lifelockcode'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		));
		register_sidebar(array(
			'name' => __('footer-3', 'lifelockcode'),
			'id' => 'footer-3',
			'description' => __('Add footer-3 here to appear in footer.', 'lifelockcode'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		));
		register_sidebar(array(
			'name' => __('footer-4', 'lifelockcode'),
			'id' => 'footer-4',
			'description' => __('Add footer-4 here to appear in footer.', 'lifelockcode'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		));
		register_sidebar(array(
			'name' => __('footer-bot', 'lifelockcode'),
			'id' => 'footer-bot',
			'description' => __('Add footer-bot here to appear in footer.', 'lifelockcode'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		));
		register_sidebar(array(
			'name' => __('footer-bot-product', 'lifelockcode'),
			'id' => 'footer-bot-product',
			'description' => __('Add text here to appear above footer on product page.', 'lifelockcode'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		));

	}

	public function custom_rewrite_rule() {
		add_rewrite_rule('^promo\/([^\/]*)\/?', 'index.php?pagename=promo', 'top');
		add_rewrite_rule('^price\/([^\/]*)\/?', 'index.php?pagename=pricing', 'top');
		add_rewrite_rule('^price\/?', 'index.php?pagename=pricing', 'top');
		add_rewrite_rule('^pricing\/([^\/]*)\/?', 'index.php?pagename=pricing', 'top');
		add_rewrite_rule('^products\/([^\/]*)\/?', 'index.php?pagename=products', 'top');
		add_rewrite_rule('^compare\/([^\/]*)\/?', 'index.php?pagename=compare', 'top');
		add_rewrite_rule('^home\/([^\/]*)\/?', 'index.php?pagename=home', 'top');
		flush_rewrite_rules(false);
	}
	public function getData($url) {
		$html = file_get_contents($url);

		$crawlers = new Crawler($html);
		$crawler_comment = $crawlers->filter('#tab-all > ul > li')->each(function (Crawler $comment) {
			return $comment;
		});

		$crawler_totalreviews = $crawlers->filter('#content')->each(function (Crawler $totalreviews) {
			return $totalreviews;
		});
		$reviews = [];
		foreach ($crawler_comment as $comment) {
			$review = [];
			$review['title'] = $comment->filter('div.generalBoxRight h2')->text();
			$review['review'] = $comment->filter('div.generalBoxRight p.description')->text();
			$review['str'] = str_replace('Read more »', ' ', $review['review']);
			$review['reviewDetail'] = $comment->filter('div.generalBoxRight p.reviewDetail')->html();
			$review['star'] = $comment->filter('div.starRating > span')->attr('class');
			array_push($reviews, $review);
		}

		foreach ($crawler_totalreviews as $totalreviews) {
			$review = [];
			$review['ratingCount'] = $totalreviews->filter('.totalReviews > h2')->text();
			$review['avgReviews'] = $totalreviews->filter('.avgReviews > h2')->text();
			$review['chartReview'] = $totalreviews->filter('#ratingTable')->html();
		}

		return $reviews;
	}
}
new App;

if (!function_exists('lifelockcode_the_custom_logo')):
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 * @since Twenty Fifteen 1.5
 */
	function lifelockcode_the_custom_logo() {
		if (function_exists('the_custom_logo')) {
			the_custom_logo();
		}
	}
endif;

if (!function_exists('lifelockcode_the_custom_menu')) {
	function lifelockcode_the_custom_menu($slug) {
		$menu = array(
			'theme_location' => $slug,
			'container' => 'nav',
			'container_class' => $slug,
		);
		wp_nav_menu($menu);
	}
}

function lifelockcode_categorized_blog() {
	if (false === ($all_the_cool_cats = get_transient('lifelockcode_categories'))) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories(array(
			'fields' => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number' => 2,
		));

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count($all_the_cool_cats);

		set_transient('lifelockcode_categories', $all_the_cool_cats);
	}

	if ($all_the_cool_cats > 1) {
		// This blog has more than 1 category so lifelockcode_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so lifelockcode_categorized_blog should return false.
		return false;
	}
}

//=====================================garung - 20161216 =================================
function render_url($code_id, $post_id = '') {
	$a = '107';
	$c = '28';
	$s1 = 'dmn';
	$s2 = 'llcode';
	$arr_s4 = ['google:g', 'msn:m', 'yahoo:m', 'others:u'];
	if (!$_SESSION['referrer']) {
		$str_referer = $_SERVER['HTTP_REFERER'];
		$_SESSION['referrer'] = $str_referer;
	}

	$s4 = '';
	$klast = 'u';
	$get_referer = $_SESSION['referrer'];
	foreach ($arr_s4 as $key => $val_s4) {
		$explode = explode(':', $val_s4, 2);
		if ((strpos($get_referer, $explode[0]) !== FALSE) AND ($explode[0] !== 'others')) {
			$s4 = $explode[1];
		}
		if ($explode[0] == 'others') {
			$klast = $explode[1];
		}
	}
	if ($s4 == '') {
		$s4 = $klast;
	}
	$tmp_s3 = get_post_meta($code_id, 'code', true);
	if (!empty($tmp_s3)) {
		$s3 = $tmp_s3;
	} else {
		$s3 = '';
	}
	if (!empty($post_id)) {
		$get_post = get_post($post_id);
	} else {
		$get_post = get_post();
	}
	if (!empty($get_post)) {
		$s5 = $get_post->post_name;
	} else {
		$s5 = '';
	}
	if (($s5 == 'promo') || ($s5 == 'price') || ($s5 == 'pricing') || ($s5 == 'compare') || ($s5 == 'product')) {
		$expl = explode('/', $_SERVER['REQUEST_URI']);
		if (!empty($expl[2])) {
			$s5 = $expl[2];
		}
	}
	$str_url = 'http://lifelocktrk.com/?a=' . $a . '&c=' . $c . '&s1=' . $s1 . '&s2=' . $s2 . '&s3=' . $s3 . '&s4=' . $s4 . '&s5=' . $s5;
	return $str_url;
}
//==================== Ngoc
function custompricing($code_id, $post_type, $post_id = '') {
	$a = get_post_meta($code_id, 'code', true);
	$b = get_post($post_id);
	$c = get_post_meta($code_id, $post_type);

}