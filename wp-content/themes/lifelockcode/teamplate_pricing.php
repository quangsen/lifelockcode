<?php
/*
Template Name: Pricing
*/
get_header(); ?>
<?php while ( have_posts() ) : the_post();
    $title = get_the_title();
    $i = 1;
    $arr_infoproduct = [];
    $current_post_id = get_the_ID();
    $info_products = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1,) ); 
    while($info_products->have_posts() ) : 
        $info_products->the_post();
        $arr_infoproduct[get_the_title()]['price_month'] = (float)get_field('price_month', get_the_ID());
        $arr_infoproduct[get_the_title()]['price_annual'] = (float)get_field('price_annual', get_the_ID());
        $arr_infoproduct[get_the_title()]['id'] = get_the_ID();
    endwhile;
?>

<div class="innerCont w1354 kenpricing">
    <div class="largeTitle">
        <h1><?php echo $title; ?></h1>
    </div>
    <div class="box-shadow LifeLock-Pricing">
        <h2><span style="font-size: 1.17em;">LifeLock Pricing</span></h2>
        <div class="pricing-select">
            <h5>Select a Promo Code:</h5>
            <li class="dropdown aa">
                <a href="#" data-toggle="dropdown" class="dropp-header__btn js-dropp-action custom-replace" title="">LLC3015 - 15% Off + 30 Days Risk Free*<i class="icon"></i></a>
                <ul class="dropdown-menu1 custom-list">
                    <?php $loop = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> -1,) ); ?>
                    <?php $i=1; ?>
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <?php 
                        $max_discount = get_post_meta(get_the_ID(), 'discount', true);
                        $max_term = get_post_meta(get_the_ID(), 'term', true);
                        if(!$max_discount) {
                            $max_discount = 0;
                        }
                        if(!$max_term) {
                            $max_term = 0;
                        }
                    ?>
                    <li value="llc<?php echo $i ?>" class="abc llc<?php echo $i ?> custom-list-<?php echo $i; ?>" data-title='<?php echo get_the_title(); ?>' data-url='<?php echo render_url(get_the_ID(), $current_post_id); ?>' max_discount='<?php echo $max_discount; ?>' max_term='<?php echo $max_term; ?>'><?php the_title(); ?> <?php the_field('promotion') ?></li>
                    <?php $i++; ?>
                    <?php endwhile; ?>
                </ul>
            </li>
             
              
            <p>Prices Include Promo Code: <span style="font-size: 1.17em;" class="pri-title"></span></p>

                        </div>
                        <div class="row">
                <div class="col-sm-4">
                    <div class="ricing-title" style="min-height: auto;">
                        <div class="logo-lifelock-text"  >
                            <div class="logo"><img src="<?php bloginfo('template_directory'); ?>/image/logo-icon-text.gif" alt="price of LifeLock" width="37" height="36"></div>
                            <div class="text">
                                <p>Life<span>Lock</span></p>
                                <p>Standard ™</p>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="ricing-title"  style="min-height: auto;">
                            <div class="logo-lifelock-text">
                                <div class="logo"><img src="<?php bloginfo('template_directory'); ?>/image/logo-icon-text.gif" alt="price of LifeLock" width="37" height="36"></div>
                                <div class="text">
                                    <p>Life<span>Lock</span></p>
                                    <p>Advantage ™</p>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="ricing-title"  style="min-height: auto;">
                                <div class="logo-lifelock-text">
                                    <div class="logo"><img src="<?php bloginfo('template_directory'); ?>/image/logo-icon-text.gif" alt="price of LifeLock" width="37" height="36"></div>
                                    <div class="text">
                                        <p>Life<span>Lock</span></p>
                                        <p>Ultimate plus ™</p>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                <div class="row llc1 ll3015 box" style="height: auto;">
                    <div class="col-sm-4">
                        <div class="ricing-header" style="text-align: center; background-color: #737070;">
                        <div class="custom-price-1"></div>
                        <a href="" class="pri-a" title="Enroll Button 1"><img src="<?php bloginfo('template_directory'); ?>/image/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a></div>
                    </div>
                    <div class="col-sm-4">
                        <div class="ricing-header" style="text-align: center; background-color: #737070;">
                            <div class="custom-price-2"></div>
                        <a href="" class="pri-a" title="Enroll Button 1"><img src="<?php bloginfo('template_directory'); ?>/image/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a> </div>  
                    </div>
                    <div class="col-sm-4">
                        <div class="ricing-header" style="text-align: center; background-color: #737070;">
                        <div class="custom-price-3"></div>
                        <a href="" class="pri-a" title="Enroll Button 1"><img src="<?php bloginfo('template_directory'); ?>/image/enroll-button-1.png" class="btn-img-hover" alt="Life Lock promo code"><br></a> </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="ricing-content">
                            <?php
                                    $colors = get_field('option',$arr_infoproduct['STANDARD']['id']);
                                    if( $colors ): ?>
                                        <?php foreach( $colors as $color ): ?>
                                            <p><?php echo $color; ?></p>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                            <div class="ricing-content">
                                 <?php
                                    $colors = get_field('option',$arr_infoproduct['ADVANTAGE']['id']);
                                    if( $colors ): ?>
                                        <?php foreach( $colors as $color ): ?>
                                            <p><?php echo $color; ?></p>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="ricing-content">
                             <?php
                            $colors = get_field('option',$arr_infoproduct['ULTIMATE PLUS']['id']);
                            if( $colors ): ?>
                                <?php foreach( $colors as $color ): ?>
                                    <p><?php echo $color; ?></p>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

            <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.custom-list').hide();
    detail_code('.custom-list-1');
    $(".abc").click(function(){
            
            detail_code(this);
    });    

    $( 'a[data-toggle="dropdown"]' ).click( function() {
        $( 'ul.dropdown-menu1' ).slideToggle();
    } );

    $( document ).click( function(e) {
        if( e.target !== document.querySelectorAll( 'a[data-toggle="dropdown"]' )[0] && e.target !== document.querySelectorAll( 'a[data-toggle="dropdown"] i' )[0] ) {
            $( 'ul.dropdown-menu1' ).hide();     
        }
    } );

    $('.abc').click(function(){
        var content = $(this).text();
        console.log(content);
        $('.custom-replace').html(content+'<a href="#" class="dropp-header__btn js-dropp-action" title=""><i class="icon"></i></a>');
    });

});
function detail_code(tag) {
    var title = $(tag).attr('data-title');
    var return_url = $(tag).attr('data-url');
    var max_discount = $(tag).attr('max_discount');
    var max_term = $(tag).attr('max_term');
    var str_show = '<a href="'+return_url+'" style="corlor: #333;" title="'+title+'">'+title+'</a> ('+max_discount+' % Off + '+max_term+' Days Risk Free*)';

    var standard_month = <?php echo (!empty($arr_infoproduct['STANDARD']['price_month']) ? $arr_infoproduct['STANDARD']['price_month'] : 0); ?>;
    var standard_annual = <?php echo (!empty($arr_infoproduct['STANDARD']['price_annual']) ? $arr_infoproduct['STANDARD']['price_annual'] : 0); ?>;
    var junior_month = <?php echo (!empty($arr_infoproduct['JUNIOR']['price_month']) ? $arr_infoproduct['JUNIOR']['price_month'] : 0); ?>;
    var junior_annual = <?php echo (!empty($arr_infoproduct['JUNIOR']['price_annual']) ? $arr_infoproduct['JUNIOR']['price_annual'] : 0); ?>;
    var ultimate_month = <?php echo (!empty($arr_infoproduct['ULTIMATE PLUS']['price_month']) ? $arr_infoproduct['ULTIMATE PLUS']['price_month'] : 0); ?>;
    var ultimate_annual = <?php echo (!empty($arr_infoproduct['ULTIMATE PLUS']['price_annual']) ? $arr_infoproduct['ULTIMATE PLUS']['price_annual'] : 0); ?>;

    // calulating price and rounding price 
    var price_standard_month = standard_month * ((100 - (parseFloat(max_discount)))/100);
    var price_standard_annual = standard_annual * ((100 - (parseFloat(max_discount)))/100);
    var price_junior_month = junior_month * ((100 - (parseFloat(max_discount)))/100);
    var price_junior_annual = junior_annual * ((100 - (parseFloat(max_discount)))/100);
    var price_ultimate_month = ultimate_month * ((100 - (parseFloat(max_discount)))/100);
    var price_ultimate_annual = ultimate_annual * ((100 - (parseFloat(max_discount)))/100);

    if(!price_standard_month) {
        price_standard_month = 0;
    }
    if(!price_standard_annual) {
        price_standard_annual = 0;
    }
    if(!price_junior_month) {
        price_junior_month = 0;
    }
    if(!price_junior_annual) {
        price_junior_annual = 0;
    }
    if(!price_ultimate_month) {
        price_ultimate_month = 0;
    }
    if(!price_ultimate_annual) {
        price_ultimate_annual = 0;
    }
        price_standard_month = price_standard_month.toFixed(2);
        price_standard_annual = price_standard_annual.toFixed(2);
        price_junior_month = price_junior_month.toFixed(2);
        price_junior_annual = price_junior_annual.toFixed(2);
        price_ultimate_month = price_ultimate_month.toFixed(2);
        price_ultimate_annual = price_ultimate_annual.toFixed(2);

    $('.custom-price-1').html('<h3 style="color: #fff; margin-bottom: 0px; " class="month1">$'+price_standard_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_standard_annual+'/annual</h3>');
    $('.custom-price-2').html('<h3 style="color: #fff; margin-bottom: 0px;" class="month1">$'+price_junior_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_junior_annual+'/annual</h3>');
    $('.custom-price-3').html('<h3 style="color: #fff; margin-bottom: 0px;" class="month1">$'+price_ultimate_month+'/month</h3><h3 style="color: #fff;padding-top:10px;" class="annual1">$'+price_ultimate_annual+'/annual</h3>');

    $('.pri-a').attr('href', return_url);
    $('.pri-title').html(str_show);
}
</script>
            <?php endwhile; // end of the loop. ?>
            </div><!-- #content -->
            </div><!-- #primary -->
            <?php get_footer() ?>