<!DOCTYPE html>
<html>
<head>

	   <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	   <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/image/favicon.ico" />
	<?php wp_head();?>
	<script type="text/javascript">
		if(screen.width > 1200) {
			document.write('<meta name="viewport" content="width=1200">');
		} else {
			document.write('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
		}
	</script>
</head>
<body <?php body_class();?>>
<?php  
    $id_page = get_the_ID();
    $id_code = '134';
    $get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
    while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
        $id_code = get_the_ID();
    endwhile;
    $max_discount = get_post_meta($id_code, 'discount', true);
    $max_term = get_post_meta($id_code, 'term', true);
?>
<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<div class="site-branding container">
			<div class="logo col-md-6">
				<?php lifelockcode_the_custom_logo();?>
				<aside id="text-3" class="widget widget_text">			
					<div class="textwidget">
						<div class="custom">
							<p style="padding-bottom: 10px"><span style="text-align: center; font-size: 12pt;"><strong><?php the_time('F Y')?></strong></span></p>
							<h2 style="font-size: 18px;">
								<strong>
									<span style="text-align: center; font-size: 12pt;"><a href="<?php echo render_url($id_code, $id_page); ?>" title="Promo Codes"><?php the_field('name', '169');?>
									</a><br></span>
								</strong>
							</h2>
						</div>
					</div>
				</aside>			
			</div>
			<div class="main-menu col-md-6">
								<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
				<ul class="nav navbar-nav">
								<?php lifelockcode_the_custom_menu('primary-menu');?>
								<script type="text/javascript">
									// jQuery(document).ready(function(){
										var html = '<li id="menu-item-98" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-98"><a href="<?php echo render_url($id_code, $id_page); ?>"><i class="_mi _before dashicons dashicons-lock" aria-hidden="true"></i><span>Lifelock.com</span></a></li>';
										jQuery('#menu-main-menu').append(html);
									// });
								</script>
					
				</ul>
			</div><!--/.nav-collapse -->
	</nav>
				</div>
			</div><!-- .site-branding -->
		</header><!-- .site-header -->

	<div id="content" class="site-content">