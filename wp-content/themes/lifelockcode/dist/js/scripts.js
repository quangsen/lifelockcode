/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  'use strict';
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 3)) {
    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4')
  }
}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.7
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: alert.js v3.3.7
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.7'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector === '#' ? [] : selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);

/* ========================================================================
 * Bootstrap: button.js v3.3.7
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.7'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state += 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d).prop(d, true)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d).prop(d, false)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked')) changed = false
        $parent.find('.active').removeClass('active')
        this.$element.addClass('active')
      } else if ($input.prop('type') == 'checkbox') {
        if (($input.prop('checked')) !== this.$element.hasClass('active')) changed = false
        this.$element.toggleClass('active')
      }
      $input.prop('checked', this.$element.hasClass('active'))
      if (changed) $input.trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
      this.$element.toggleClass('active')
    }
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target).closest('.btn')
      Plugin.call($btn, 'toggle')
      if (!($(e.target).is('input[type="radio"], input[type="checkbox"]'))) {
        // Prevent double click on radios, and the double selections (so cancellation) on checkboxes
        e.preventDefault()
        // The target component still receive the focus
        if ($btn.is('input,button')) $btn.trigger('focus')
        else $btn.find('input:visible,button:visible').first().trigger('focus')
      }
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.7
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      = null
    this.sliding     = null
    this.interval    = null
    this.$active     = null
    this.$items      = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION  = '3.3.7'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
                || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that      = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel             = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this   = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: collapse.js v3.3.7
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

/* jshint latedef: false */

+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.7'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.7
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.7'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger($.Event('shown.bs.dropdown', relatedTarget))
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index)                                    index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);

/* ========================================================================
 * Bootstrap: modal.js v3.3.7
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.7'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (document !== e.target &&
            this.$element[0] !== e.target &&
            !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tooltip.js v3.3.7
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.7'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      if (that.$element) { // TODO: Check whether guarding this code with this `if` is really necessary.
        that.$element
          .removeAttr('aria-describedby')
          .trigger('hidden.bs.' + that.type)
      }
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var isSvg = window.SVGElement && el instanceof window.SVGElement
    // Avoid using $.offset() on SVGs since it gives incorrect results in jQuery 3.
    // See https://github.com/twbs/bootstrap/issues/20280
    var elOffset  = isBody ? { top: 0, left: 0 } : (isSvg ? null : $element.offset())
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
      that.$element = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: popover.js v3.3.7
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.7'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.7
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.7'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.7
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.7'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);

/* ========================================================================
 * Bootstrap: affix.js v3.3.7
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.7'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);

(function(g,e,b,j,c,i,k){/*! Jssor */
new(function(){});var d=g.$JssorEasing$={$EaseSwing:function(a){return-b.cos(a*b.PI)/2+.5},$EaseLinear:function(a){return a},$EaseInQuad:function(a){return a*a},$EaseOutQuad:function(a){return-a*(a-2)},$EaseInOutQuad:function(a){return(a*=2)<1?1/2*a*a:-1/2*(--a*(a-2)-1)},$EaseInCubic:function(a){return a*a*a},$EaseOutCubic:function(a){return(a-=1)*a*a+1},$EaseInOutCubic:function(a){return(a*=2)<1?1/2*a*a*a:1/2*((a-=2)*a*a+2)},$EaseInQuart:function(a){return a*a*a*a},$EaseOutQuart:function(a){return-((a-=1)*a*a*a-1)},$EaseInOutQuart:function(a){return(a*=2)<1?1/2*a*a*a*a:-1/2*((a-=2)*a*a*a-2)},$EaseInQuint:function(a){return a*a*a*a*a},$EaseOutQuint:function(a){return(a-=1)*a*a*a*a+1},$EaseInOutQuint:function(a){return(a*=2)<1?1/2*a*a*a*a*a:1/2*((a-=2)*a*a*a*a+2)},$EaseInSine:function(a){return 1-b.cos(a*b.PI/2)},$EaseOutSine:function(a){return b.sin(a*b.PI/2)},$EaseInOutSine:function(a){return-1/2*(b.cos(b.PI*a)-1)},$EaseInExpo:function(a){return a==0?0:b.pow(2,10*(a-1))},$EaseOutExpo:function(a){return a==1?1:-b.pow(2,-10*a)+1},$EaseInOutExpo:function(a){return a==0||a==1?a:(a*=2)<1?1/2*b.pow(2,10*(a-1)):1/2*(-b.pow(2,-10*--a)+2)},$EaseInCirc:function(a){return-(b.sqrt(1-a*a)-1)},$EaseOutCirc:function(a){return b.sqrt(1-(a-=1)*a)},$EaseInOutCirc:function(a){return(a*=2)<1?-1/2*(b.sqrt(1-a*a)-1):1/2*(b.sqrt(1-(a-=2)*a)+1)},$EaseInElastic:function(a){if(!a||a==1)return a;var c=.3,d=.075;return-(b.pow(2,10*(a-=1))*b.sin((a-d)*2*b.PI/c))},$EaseOutElastic:function(a){if(!a||a==1)return a;var c=.3,d=.075;return b.pow(2,-10*a)*b.sin((a-d)*2*b.PI/c)+1},$EaseInOutElastic:function(a){if(!a||a==1)return a;var c=.45,d=.1125;return(a*=2)<1?-.5*b.pow(2,10*(a-=1))*b.sin((a-d)*2*b.PI/c):b.pow(2,-10*(a-=1))*b.sin((a-d)*2*b.PI/c)*.5+1},$EaseInBack:function(a){var b=1.70158;return a*a*((b+1)*a-b)},$EaseOutBack:function(a){var b=1.70158;return(a-=1)*a*((b+1)*a+b)+1},$EaseInOutBack:function(a){var b=1.70158;return(a*=2)<1?1/2*a*a*(((b*=1.525)+1)*a-b):1/2*((a-=2)*a*(((b*=1.525)+1)*a+b)+2)},$EaseInBounce:function(a){return 1-d.$EaseOutBounce(1-a)},$EaseOutBounce:function(a){return a<1/2.75?7.5625*a*a:a<2/2.75?7.5625*(a-=1.5/2.75)*a+.75:a<2.5/2.75?7.5625*(a-=2.25/2.75)*a+.9375:7.5625*(a-=2.625/2.75)*a+.984375},$EaseInOutBounce:function(a){return a<1/2?d.$EaseInBounce(a*2)*.5:d.$EaseOutBounce(a*2-1)*.5+.5},$EaseGoBack:function(a){return 1-b.abs(2-1)},$EaseInWave:function(a){return 1-b.cos(a*b.PI*2)},$EaseOutWave:function(a){return b.sin(a*b.PI*2)},$EaseOutJump:function(a){return 1-((a*=2)<1?(a=1-a)*a*a:(a-=1)*a*a)},$EaseInJump:function(a){return(a*=2)<1?a*a*a:(a=2-a)*a*a}},f=g.$Jease$={$Swing:d.$EaseSwing,$Linear:d.$EaseLinear,$InQuad:d.$EaseInQuad,$OutQuad:d.$EaseOutQuad,$InOutQuad:d.$EaseInOutQuad,$InCubic:d.$EaseInCubic,$OutCubic:d.$EaseOutCubic,$InOutCubic:d.$EaseInOutCubic,$InQuart:d.$EaseInQuart,$OutQuart:d.$EaseOutQuart,$InOutQuart:d.$EaseInOutQuart,$InQuint:d.$EaseInQuint,$OutQuint:d.$EaseOutQuint,$InOutQuint:d.$EaseInOutQuint,$InSine:d.$EaseInSine,$OutSine:d.$EaseOutSine,$InOutSine:d.$EaseInOutSine,$InExpo:d.$EaseInExpo,$OutExpo:d.$EaseOutExpo,$InOutExpo:d.$EaseInOutExpo,$InCirc:d.$EaseInCirc,$OutCirc:d.$EaseOutCirc,$InOutCirc:d.$EaseInOutCirc,$InElastic:d.$EaseInElastic,$OutElastic:d.$EaseOutElastic,$InOutElastic:d.$EaseInOutElastic,$InBack:d.$EaseInBack,$OutBack:d.$EaseOutBack,$InOutBack:d.$EaseInOutBack,$InBounce:d.$EaseInBounce,$OutBounce:d.$EaseOutBounce,$InOutBounce:d.$EaseInOutBounce,$GoBack:d.$EaseGoBack,$InWave:d.$EaseInWave,$OutWave:d.$EaseOutWave,$OutJump:d.$EaseOutJump,$InJump:d.$EaseInJump};var a=new function(){var f=this,zb=/\S+/g,S=1,fb=2,jb=3,ib=4,nb=5,I,s=0,l=0,q=0,J=0,C=0,y=navigator,sb=y.appName,n=y.userAgent;function Ib(){if(!I){I={Rg:"ontouchstart"in g||"createTouch"in e};var a;if(y.pointerEnabled||(a=y.msPointerEnabled))I.Bd=a?"msTouchAction":"touchAction"}return I}function t(i){if(!s){s=-1;if(sb=="Microsoft Internet Explorer"&&!!g.attachEvent&&!!g.ActiveXObject){var f=n.indexOf("MSIE");s=S;q=o(n.substring(f+5,n.indexOf(";",f)));/*@cc_on J=@_jscript_version@*/;l=e.documentMode||q}else if(sb=="Netscape"&&!!g.addEventListener){var d=n.indexOf("Firefox"),b=n.indexOf("Safari"),h=n.indexOf("Chrome"),c=n.indexOf("AppleWebKit");if(d>=0){s=fb;l=o(n.substring(d+8))}else if(b>=0){var j=n.substring(0,b).lastIndexOf("/");s=h>=0?ib:jb;l=o(n.substring(j+1,b))}else{var a=/Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/i.exec(n);if(a){s=S;l=q=o(a[1])}}if(c>=0)C=o(n.substring(c+12))}else{var a=/(opera)(?:.*version|)[ \/]([\w.]+)/i.exec(n);if(a){s=nb;l=o(a[2])}}}return i==s}function p(){return t(S)}function N(){return p()&&(l<6||e.compatMode=="BackCompat")}function hb(){return t(jb)}function mb(){return t(nb)}function ab(){return hb()&&C>534&&C<535}function L(){return p()&&l<9}function cb(a){var b;return function(d){if(!b){b=a;var c=a.substr(0,1).toUpperCase()+a.substr(1);m([a].concat(["WebKit","ms","Moz","O","webkit"]),function(g,f){var e=a;if(f)e=g+c;if(d.style[e]!=k)return b=e})}return b}}var bb=cb("transform");function rb(a){return{}.toString.call(a)}var H;function Fb(){if(!H){H={};m(["Boolean","Number","String","Function","Array","Date","RegExp","Object"],function(a){H["[object "+a+"]"]=a.toLowerCase()})}return H}function m(a,d){if(rb(a)=="[object Array]"){for(var b=0;b<a.length;b++)if(d(a[b],b,a))return c}else for(var e in a)if(d(a[e],e,a))return c}function A(a){return a==j?String(a):Fb()[rb(a)]||"object"}function pb(a){for(var b in a)return c}function x(a){try{return A(a)=="object"&&!a.nodeType&&a!=a.window&&(!a.constructor||{}.hasOwnProperty.call(a.constructor.prototype,"isPrototypeOf"))}catch(b){}}function w(a,b){return{x:a,y:b}}function vb(b,a){setTimeout(b,a||0)}function F(b,d,c){var a=!b||b=="inherit"?"":b;m(d,function(c){var b=c.exec(a);if(b){var d=a.substr(0,b.index),e=a.substr(b.lastIndex+1,a.length-(b.lastIndex+1));a=d+e}});a=c+(a.indexOf(" ")!=0?" ":"")+a;return a}function eb(b,a){if(l<9)b.style.filter=a}function Bb(g,a,i){if(!J||J<9){var d=a.$ScaleX,e=a.$ScaleY,j=(a.$Rotate||0)%360,h="";if(j||d!=k||e!=k){if(d==k)d=1;if(e==k)e=1;var c=f.Tg(j/180*b.PI,d||1,e||1),i=f.Og(c,a.$OriginalWidth,a.$OriginalHeight);f.Dd(g,i.y);f.Id(g,i.x);h="progid:DXImageTransform.Microsoft.Matrix(M11="+c[0][0]+", M12="+c[0][1]+", M21="+c[1][0]+", M22="+c[1][1]+", SizingMethod='auto expand')"}var m=g.style.filter,n=new RegExp(/[\s]*progid:DXImageTransform\.Microsoft\.Matrix\([^\)]*\)/g),l=F(m,[n],h);eb(g,l)}}f.Pg=Ib;f.Jd=p;f.Ng=hb;f.tc=mb;f.V=L;f.sd=function(){return l};f.ng=function(){t();return C};f.$Delay=vb;function V(a){a.constructor===V.caller&&a.xd&&a.xd.apply(a,V.caller.arguments)}f.xd=V;f.qb=function(a){if(f.ud(a))a=e.getElementById(a);return a};function r(a){return a||g.event}f.vd=r;f.yc=function(a){a=r(a);return a.target||a.srcElement||e};f.Qd=function(a){a=r(a);return{x:a.pageX||a.clientX||0,y:a.pageY||a.clientY||0}};function B(c,d,a){if(a!==k)c.style[d]=a==k?"":a;else{var b=c.currentStyle||c.style;a=b[d];if(a==""&&g.getComputedStyle){b=c.ownerDocument.defaultView.getComputedStyle(c,j);b&&(a=b.getPropertyValue(d)||b[d])}return a}}function X(b,c,a,d){if(a!==k){if(a==j)a="";else d&&(a+="px");B(b,c,a)}else return o(B(b,c))}function h(c,a){var d=a?X:B,b;if(a&4)b=cb(c);return function(e,f){return d(e,b?b(e):c,f,a&2)}}function Cb(b){if(p()&&q<9){var a=/opacity=([^)]*)/.exec(b.style.filter||"");return a?o(a[1])/100:1}else return o(b.style.opacity||"1")}function Eb(c,a,f){if(p()&&q<9){var h=c.style.filter||"",i=new RegExp(/[\s]*alpha\([^\)]*\)/g),e=b.round(100*a),d="";if(e<100||f)d="alpha(opacity="+e+") ";var g=F(h,[i],d);eb(c,g)}else c.style.opacity=a==1?"":b.round(a*100)/100}var xb={$Rotate:["rotate"],$RotateX:["rotateX"],$RotateY:["rotateY"],$ScaleX:["scaleX",2],$ScaleY:["scaleY",2],$TranslateX:["translateX",1],$TranslateY:["translateY",1],$TranslateZ:["translateZ",1],$SkewX:["skewX"],$SkewY:["skewY"]};function Z(e,c){if(p()&&l&&l<10){delete c.$RotateX;delete c.$RotateY}var d=bb(e);if(d){var b="";a.c(c,function(e,c){var a=xb[c];if(a){var d=a[1]||0;b+=(b?" ":"")+a[0]+"("+e+(["deg","px",""])[d]+")"}});e.style[d]=b}}f.jg=function(b,a){if(ab())vb(f.K(j,Z,b,a));else(L()?Bb:Z)(b,a)};f.Sc=h("transformOrigin",4);f.ig=h("backfaceVisibility",4);f.kg=h("transformStyle",4);f.mg=h("perspective",6);f.lg=h("perspectiveOrigin",4);f.sg=function(a,c){if(p()&&q<9||q<10&&N())a.style.zoom=c==1?"":c;else{var b=bb(a);if(b){var f="scale("+c+")",e=a.style[b],g=new RegExp(/[\s]*scale\(.*?\)/g),d=F(e,[g],f);a.style[b]=d}}};f.Ib=function(b,a){return function(c){c=r(c);var e=c.type,d=c.relatedTarget||(e=="mouseout"?c.toElement:c.fromElement);(!d||d!==a&&!f.zg(a,d))&&b(c)}};f.e=function(a,c,d,b){a=f.qb(a);if(a.addEventListener){c=="mousewheel"&&a.addEventListener("DOMMouseScroll",d,b);a.addEventListener(c,d,b)}else if(a.attachEvent){a.attachEvent("on"+c,d);b&&a.setCapture&&a.setCapture()}};f.R=function(a,c,d,b){a=f.qb(a);if(a.removeEventListener){c=="mousewheel"&&a.removeEventListener("DOMMouseScroll",d,b);a.removeEventListener(c,d,b)}else if(a.detachEvent){a.detachEvent("on"+c,d);b&&a.releaseCapture&&a.releaseCapture()}};f.bc=function(a){a=r(a);a.preventDefault&&a.preventDefault();a.cancel=c;a.returnValue=i};f.Kg=function(a){a=r(a);a.stopPropagation&&a.stopPropagation();a.cancelBubble=c};f.K=function(d,c){var a=[].slice.call(arguments,2),b=function(){var b=a.concat([].slice.call(arguments,0));return c.apply(d,b)};return b};f.ug=function(a,b){if(b==k)return a.textContent||a.innerText;var c=e.createTextNode(b);f.sc(a);a.appendChild(c)};f.O=function(d,c){for(var b=[],a=d.firstChild;a;a=a.nextSibling)(c||a.nodeType==1)&&b.push(a);return b};function qb(a,c,e,b){b=b||"u";for(a=a?a.firstChild:j;a;a=a.nextSibling)if(a.nodeType==1){if(R(a,b)==c)return a;if(!e){var d=qb(a,c,e,b);if(d)return d}}}f.D=qb;function P(a,d,f,b){b=b||"u";var c=[];for(a=a?a.firstChild:j;a;a=a.nextSibling)if(a.nodeType==1){R(a,b)==d&&c.push(a);if(!f){var e=P(a,d,f,b);if(e.length)c=c.concat(e)}}return c}function kb(a,c,d){for(a=a?a.firstChild:j;a;a=a.nextSibling)if(a.nodeType==1){if(a.tagName==c)return a;if(!d){var b=kb(a,c,d);if(b)return b}}}f.xg=kb;function db(a,c,e){var b=[];for(a=a?a.firstChild:j;a;a=a.nextSibling)if(a.nodeType==1){(!c||a.tagName==c)&&b.push(a);if(!e){var d=db(a,c,e);if(d.length)b=b.concat(d)}}return b}f.vg=db;f.tg=function(b,a){return b.getElementsByTagName(a)};function z(){var e=arguments,d,c,b,a,g=1&e[0],f=1+g;d=e[f-1]||{};for(;f<e.length;f++)if(c=e[f])for(b in c){a=c[b];if(a!==k){a=c[b];var h=d[b];d[b]=g&&(x(h)||x(a))?z(g,{},h,a):a}}return d}f.p=z;function W(f,g){var d={},c,a,b;for(c in f){a=f[c];b=g[c];if(a!==b){var e;if(x(a)&&x(b)){a=W(a,b);e=!pb(a)}!e&&(d[c]=a)}}return d}f.fd=function(a){return A(a)=="function"};f.uc=function(a){return A(a)=="array"};f.ud=function(a){return A(a)=="string"};f.Zb=function(a){return!isNaN(o(a))&&isFinite(a)};f.c=m;f.yg=x;function O(a){return e.createElement(a)}f.mb=function(){return O("DIV")};f.Cg=function(){return O("SPAN")};f.kd=function(){};function T(b,c,a){if(a==k)return b.getAttribute(c);b.setAttribute(c,a)}function R(a,b){return T(a,b)||T(a,"data-"+b)}f.C=T;f.j=R;function u(b,a){if(a==k)return b.className;b.className=a}f.Zc=u;function ub(b){var a={};m(b,function(b){a[b]=b});return a}function wb(b,a){return b.match(a||zb)}function M(b,a){return ub(wb(b||"",a))}f.Bg=wb;function Y(b,c){var a="";m(c,function(c){a&&(a+=b);a+=c});return a}function E(a,c,b){u(a,Y(" ",z(W(M(u(a)),M(c)),M(b))))}f.Yc=function(a){return a.parentNode};f.S=function(a){f.Y(a,"none")};f.A=function(a,b){f.Y(a,b?"none":"")};f.qg=function(b,a){b.removeAttribute(a)};f.rg=function(){return p()&&l<10};f.pg=function(d,c){if(c)d.style.clip="rect("+b.round(c.$Top)+"px "+b.round(c.$Right)+"px "+b.round(c.$Bottom)+"px "+b.round(c.$Left)+"px)";else{var g=d.style.cssText,f=[new RegExp(/[\s]*clip: rect\(.*?\)[;]?/i),new RegExp(/[\s]*cliptop: .*?[;]?/i),new RegExp(/[\s]*clipright: .*?[;]?/i),new RegExp(/[\s]*clipbottom: .*?[;]?/i),new RegExp(/[\s]*clipleft: .*?[;]?/i)],e=F(g,f,"");a.Nb(d,e)}};f.T=function(){return+new Date};f.H=function(b,a){b.appendChild(a)};f.Pb=function(b,a,c){(c||a.parentNode).insertBefore(b,a)};f.Hb=function(a,b){(b||a.parentNode).removeChild(a)};f.Jg=function(a,b){m(a,function(a){f.Hb(a,b)})};f.sc=function(a){f.Jg(f.O(a,c),a)};f.Oe=function(a,b){var c=f.Yc(a);b&1&&f.E(a,(f.l(c)-f.l(a))/2);b&2&&f.G(a,(f.m(c)-f.m(a))/2)};f.Kb=function(b,a){return parseInt(b,a||10)};var o=parseFloat;f.Nc=o;f.zg=function(b,a){var c=e.body;while(a&&b!==a&&c!==a)try{a=a.parentNode}catch(d){return i}return b===a};function U(d,c,b){var a=d.cloneNode(!c);!b&&f.qg(a,"id");return a}f.X=U;f.Cb=function(e,g){var a=new Image;function b(e,c){f.R(a,"load",b);f.R(a,"abort",d);f.R(a,"error",d);g&&g(a,c)}function d(a){b(a,c)}if(mb()&&l<11.6||!e)b(!e);else{f.e(a,"load",b);f.e(a,"abort",d);f.e(a,"error",d);a.src=e}};f.Ud=function(d,a,e){var c=d.length+1;function b(b){c--;if(a&&b&&b.src==a.src)a=b;!c&&e&&e(a)}m(d,function(a){f.Cb(a.src,b)});b()};f.Xc=function(b,g,i,h){if(h)b=U(b);var c=P(b,g);if(!c.length)c=a.tg(b,g);for(var f=c.length-1;f>-1;f--){var d=c[f],e=U(i);u(e,u(d));a.Nb(e,d.style.cssText);a.Pb(e,d);a.Hb(d)}return b};function Gb(b){var l=this,p="",r=["av","pv","ds","dn"],g=[],q,j=0,h=0,d=0;function i(){E(b,q,g[d||j||h&2||h]);a.W(b,"pointer-events",d?"none":"")}function c(){j=0;i();f.R(e,"mouseup",c);f.R(e,"touchend",c);f.R(e,"touchcancel",c)}function o(a){if(d)f.bc(a);else{j=4;i();f.e(e,"mouseup",c);f.e(e,"touchend",c);f.e(e,"touchcancel",c)}}l.jd=function(a){if(a===k)return h;h=a&2||a&1;i()};l.$Enable=function(a){if(a===k)return!d;d=a?0:3;i()};l.$Elmt=b=f.qb(b);var n=a.Bg(u(b));if(n)p=n.shift();m(r,function(a){g.push(p+a)});q=Y(" ",g);g.unshift("");f.e(b,"mousedown",o);f.e(b,"touchstart",o)}f.ac=function(a){return new Gb(a)};f.W=B;f.ib=h("overflow");f.G=h("top",2);f.E=h("left",2);f.l=h("width",2);f.m=h("height",2);f.Id=h("marginLeft",2);f.Dd=h("marginTop",2);f.z=h("position");f.Y=h("display");f.J=h("zIndex",1);f.Ab=function(b,a,c){if(a!=k)Eb(b,a,c);else return Cb(b)};f.Nb=function(a,b){if(b!=k)a.style.cssText=b;else return a.style.cssText};var Q={$Opacity:f.Ab,$Top:f.G,$Left:f.E,N:f.l,P:f.m,Bb:f.z,Kh:f.Y,$ZIndex:f.J},K;function G(){if(!K)K=z({Mh:f.Dd,Lh:f.Id,$Clip:f.pg,B:f.jg},Q);return K}function ob(){var a={};a.B=a.B;a.B=a.$Rotate;a.B=a.$RotateX;a.B=a.$RotateY;a.B=a.$SkewX;a.B=a.$SkewY;a.B=a.$TranslateX;a.B=a.$TranslateY;a.B=a.$TranslateZ;return G()}f.ne=G;f.Pc=ob;f.xe=function(c,b){G();var a={};m(b,function(d,b){if(Q[b])a[b]=Q[b](c)});return a};f.bb=function(c,b){var a=G();m(b,function(d,b){a[b]&&a[b](c,d)})};f.Wd=function(b,a){ob();f.bb(b,a)};var D=new function(){var a=this;function b(d,g){for(var j=d[0].length,i=d.length,h=g[0].length,f=[],c=0;c<i;c++)for(var k=f[c]=[],b=0;b<h;b++){for(var e=0,a=0;a<j;a++)e+=d[c][a]*g[a][b];k[b]=e}return f}a.$ScaleX=function(b,c){return a.Vc(b,c,0)};a.$ScaleY=function(b,c){return a.Vc(b,0,c)};a.Vc=function(a,c,d){return b(a,[[c,0],[0,d]])};a.Ub=function(d,c){var a=b(d,[[c.x],[c.y]]);return w(a[0][0],a[1][0])}};f.Tg=function(d,a,c){var e=b.cos(d),f=b.sin(d);return[[e*a,-f*c],[f*a,e*c]]};f.Og=function(d,c,a){var e=D.Ub(d,w(-c/2,-a/2)),f=D.Ub(d,w(c/2,-a/2)),g=D.Ub(d,w(c/2,a/2)),h=D.Ub(d,w(-c/2,a/2));return w(b.min(e.x,f.x,g.x,h.x)+c/2,b.min(e.y,f.y,g.y,h.y)+a/2)};var yb={$Zoom:1,$ScaleX:1,$ScaleY:1,$Rotate:0,$RotateX:0,$RotateY:0,$TranslateX:0,$TranslateY:0,$TranslateZ:0,$SkewX:0,$SkewY:0};f.Lc=function(b){var c=b||{};if(b)if(a.fd(b))c={kb:c};else if(a.fd(b.$Clip))c.$Clip={kb:b.$Clip};return c};function tb(c,a){var b={};m(c,function(c,d){var e=c;if(a[d]!=k)if(f.Zb(c))e=c+a[d];else e=tb(c,a[d]);b[d]=e});return b}f.Je=tb;f.Kd=function(h,i,w,n,y,z,o){var c=i;if(h){c={};for(var g in i){var A=z[g]||1,v=y[g]||[0,1],e=(w-v[0])/v[1];e=b.min(b.max(e,0),1);e=e*A;var u=b.floor(e);if(e!=u)e-=u;var l=n.kb||d.$EaseSwing,m,B=h[g],q=i[g];if(a.Zb(q)){l=n[g]||l;var x=l(e);m=B+q*x}else{m=a.p({wb:{}},h[g]);a.c(q.wb||q,function(d,a){if(n.$Clip)l=n.$Clip[a]||n.$Clip.kb||l;var c=l(e),b=d*c;m.wb[a]=b;m[a]+=b})}c[g]=m}var t,f={$OriginalWidth:o.$OriginalWidth,$OriginalHeight:o.$OriginalHeight};a.c(yb,function(d,a){t=t||i[a];var b=c[a];if(b!=k){if(b!=d)f[a]=b;delete c[a]}else if(h[a]!=k&&h[a]!=d)f[a]=h[a]});if(i.$Zoom&&f.$Zoom){f.$ScaleX=f.$Zoom;f.$ScaleY=f.$Zoom}c.B=f}if(i.$Clip&&o.$Move){var p=c.$Clip.wb,s=(p.$Top||0)+(p.$Bottom||0),r=(p.$Left||0)+(p.$Right||0);c.$Left=(c.$Left||0)+r;c.$Top=(c.$Top||0)+s;c.$Clip.$Left-=r;c.$Clip.$Right-=r;c.$Clip.$Top-=s;c.$Clip.$Bottom-=s}if(c.$Clip&&a.rg()&&!c.$Clip.$Top&&!c.$Clip.$Left&&c.$Clip.$Right==o.$OriginalWidth&&c.$Clip.$Bottom==o.$OriginalHeight)c.$Clip=j;return c}};function m(){var b=this,d=[];function i(a,b){d.push({vc:a,Ec:b})}function h(b,c){a.c(d,function(a,e){a.vc==b&&a.Ec===c&&d.splice(e,1)})}b.$On=b.addEventListener=i;b.$Off=b.removeEventListener=h;b.n=function(b){var c=[].slice.call(arguments,1);a.c(d,function(a){a.vc==b&&a.Ec.apply(g,c)})}}var l=function(y,C,k,P,N,J){y=y||0;var d=this,q,n,o,v,z=0,H,I,G,B,x=0,h=0,m=0,D,l,f,e,p,w=[],A;function O(a){f+=a;e+=a;l+=a;h+=a;m+=a;x+=a}function u(n){var g=n;if(p&&(g>=e||g<=f))g=((g-f)%p+p)%p+f;if(!D||v||h!=g){var i=b.min(g,e);i=b.max(i,f);if(!D||v||i!=m){if(J){var j=(i-l)/(C||1);if(k.$Reverse)j=1-j;var o=a.Kd(N,J,j,H,G,I,k);a.c(o,function(b,a){A[a]&&A[a](P,b)})}d.Ic(m-l,i-l);m=i;a.c(w,function(b,c){var a=n<h?w[w.length-c-1]:b;a.v(m-x)});var r=h,q=m;h=g;D=c;d.Qb(r,q)}}}function E(a,c,d){c&&a.$Shift(e);if(!d){f=b.min(f,a.Fc()+x);e=b.max(e,a.gb()+x)}w.push(a)}var r=g.requestAnimationFrame||g.webkitRequestAnimationFrame||g.mozRequestAnimationFrame||g.msRequestAnimationFrame;if(a.Ng()&&a.sd()<7)r=j;r=r||function(b){a.$Delay(b,k.$Interval)};function K(){if(q){var d=a.T(),e=b.min(d-z,k.Uc),c=h+e*o;z=d;if(c*o>=n*o)c=n;u(c);if(!v&&c*o>=n*o)L(B);else r(K)}}function t(g,i,j){if(!q){q=c;v=j;B=i;g=b.max(g,f);g=b.min(g,e);n=g;o=n<h?-1:1;d.Od();z=a.T();r(K)}}function L(a){if(q){v=q=B=i;d.Ld();a&&a()}}d.$Play=function(a,b,c){t(a?h+a:e,b,c)};d.Cd=t;d.rb=L;d.Ke=function(a){t(a)};d.db=function(){return h};d.Sd=function(){return n};d.yb=function(){return m};d.v=u;d.$Move=function(a){u(h+a)};d.$IsPlaying=function(){return q};d.Ae=function(a){p=a};d.$Shift=O;d.I=function(a,b){E(a,0,b)};d.Oc=function(a){E(a,1)};d.ye=function(a){e+=a};d.Fc=function(){return f};d.gb=function(){return e};d.Qb=d.Od=d.Ld=d.Ic=a.kd;d.rc=a.T();k=a.p({$Interval:16,Uc:50},k);p=k.Tc;A=a.p({},a.ne(),k.xc);f=l=y;e=y+C;I=k.$Round||{};G=k.$During||{};H=a.Lc(k.$Easing)};var o=g.$JssorSlideshowFormations$=new function(){var h=this,d=0,a=1,f=2,e=3,s=1,r=2,t=4,q=8,w=256,x=512,v=1024,u=2048,j=u+s,i=u+r,o=x+s,m=x+r,n=w+t,k=w+q,l=v+t,p=v+q;function y(a){return(a&r)==r}function z(a){return(a&t)==t}function g(b,a,c){c.push(a);b[a]=b[a]||[];b[a].push(c)}h.$FormationStraight=function(f){for(var d=f.$Cols,e=f.$Rows,s=f.$Assembly,t=f.hc,r=[],a=0,b=0,p=d-1,q=e-1,h=t-1,c,b=0;b<e;b++)for(a=0;a<d;a++){switch(s){case j:c=h-(a*e+(q-b));break;case l:c=h-(b*d+(p-a));break;case o:c=h-(a*e+b);case n:c=h-(b*d+a);break;case i:c=a*e+b;break;case k:c=b*d+(p-a);break;case m:c=a*e+(q-b);break;default:c=b*d+a}g(r,c,[b,a])}return r};h.$FormationSwirl=function(q){var x=q.$Cols,y=q.$Rows,B=q.$Assembly,w=q.hc,A=[],z=[],u=0,b=0,h=0,r=x-1,s=y-1,t,p,v=0;switch(B){case j:b=r;h=0;p=[f,a,e,d];break;case l:b=0;h=s;p=[d,e,a,f];break;case o:b=r;h=s;p=[e,a,f,d];break;case n:b=r;h=s;p=[a,e,d,f];break;case i:b=0;h=0;p=[f,d,e,a];break;case k:b=r;h=0;p=[a,f,d,e];break;case m:b=0;h=s;p=[e,d,f,a];break;default:b=0;h=0;p=[d,f,a,e]}u=0;while(u<w){t=h+","+b;if(b>=0&&b<x&&h>=0&&h<y&&!z[t]){z[t]=c;g(A,u++,[h,b])}else switch(p[v++%p.length]){case d:b--;break;case f:h--;break;case a:b++;break;case e:h++}switch(p[v%p.length]){case d:b++;break;case f:h++;break;case a:b--;break;case e:h--}}return A};h.$FormationZigZag=function(p){var w=p.$Cols,x=p.$Rows,z=p.$Assembly,v=p.hc,t=[],u=0,b=0,c=0,q=w-1,r=x-1,y,h,s=0;switch(z){case j:b=q;c=0;h=[f,a,e,a];break;case l:b=0;c=r;h=[d,e,a,e];break;case o:b=q;c=r;h=[e,a,f,a];break;case n:b=q;c=r;h=[a,e,d,e];break;case i:b=0;c=0;h=[f,d,e,d];break;case k:b=q;c=0;h=[a,f,d,f];break;case m:b=0;c=r;h=[e,d,f,d];break;default:b=0;c=0;h=[d,f,a,f]}u=0;while(u<v){y=c+","+b;if(b>=0&&b<w&&c>=0&&c<x&&typeof t[y]=="undefined"){g(t,u++,[c,b]);switch(h[s%h.length]){case d:b++;break;case f:c++;break;case a:b--;break;case e:c--}}else{switch(h[s++%h.length]){case d:b--;break;case f:c--;break;case a:b++;break;case e:c++}switch(h[s++%h.length]){case d:b++;break;case f:c++;break;case a:b--;break;case e:c--}}}return t};h.$FormationStraightStairs=function(q){var u=q.$Cols,v=q.$Rows,e=q.$Assembly,t=q.hc,r=[],s=0,c=0,d=0,f=u-1,h=v-1,x=t-1;switch(e){case j:case m:case o:case i:var a=0,b=0;break;case k:case l:case n:case p:var a=f,b=0;break;default:e=p;var a=f,b=0}c=a;d=b;while(s<t){if(z(e)||y(e))g(r,x-s++,[d,c]);else g(r,s++,[d,c]);switch(e){case j:case m:c--;d++;break;case o:case i:c++;d--;break;case k:case l:c--;d--;break;case p:case n:default:c++;d++}if(c<0||d<0||c>f||d>h){switch(e){case j:case m:a++;break;case k:case l:case o:case i:b++;break;case p:case n:default:a--}if(a<0||b<0||a>f||b>h){switch(e){case j:case m:a=f;b++;break;case o:case i:b=h;a++;break;case k:case l:b=h;a--;break;case p:case n:default:a=0;b++}if(b>h)b=h;else if(b<0)b=0;else if(a>f)a=f;else if(a<0)a=0}d=b;c=a}}return r};h.$FormationSquare=function(i){var a=i.$Cols||1,c=i.$Rows||1,j=[],d,e,f,h,k;f=a<c?(c-a)/2:0;h=a>c?(a-c)/2:0;k=b.round(b.max(a/2,c/2))+1;for(d=0;d<a;d++)for(e=0;e<c;e++)g(j,k-b.min(d+1+f,e+1+h,a-d+f,c-e+h),[e,d]);return j};h.$FormationRectangle=function(f){var d=f.$Cols||1,e=f.$Rows||1,h=[],a,c,i;i=b.round(b.min(d/2,e/2))+1;for(a=0;a<d;a++)for(c=0;c<e;c++)g(h,i-b.min(a+1,c+1,d-a,e-c),[c,a]);return h};h.$FormationRandom=function(d){for(var e=[],a,c=0;c<d.$Rows;c++)for(a=0;a<d.$Cols;a++)g(e,b.ceil(1e5*b.random())%13,[c,a]);return e};h.$FormationCircle=function(d){for(var e=d.$Cols||1,f=d.$Rows||1,h=[],a,i=e/2-.5,j=f/2-.5,c=0;c<e;c++)for(a=0;a<f;a++)g(h,b.round(b.sqrt(b.pow(c-i,2)+b.pow(a-j,2))),[a,c]);return h};h.$FormationCross=function(d){for(var e=d.$Cols||1,f=d.$Rows||1,h=[],a,i=e/2-.5,j=f/2-.5,c=0;c<e;c++)for(a=0;a<f;a++)g(h,b.round(b.min(b.abs(c-i),b.abs(a-j))),[a,c]);return h};h.$FormationRectangleCross=function(f){for(var h=f.$Cols||1,i=f.$Rows||1,j=[],a,d=h/2-.5,e=i/2-.5,k=b.max(d,e)+1,c=0;c<h;c++)for(a=0;a<i;a++)g(j,b.round(k-b.max(d-b.abs(c-d),e-b.abs(a-e)))-1,[a,c]);return j}};g.$JssorSlideshowRunner$=function(n,s,q,t,y){var f=this,u,g,e,x=0,w=t.$TransitionsOrder,r,h=8;function k(g,f){var e={$Interval:f,$Duration:1,$Delay:0,$Cols:1,$Rows:1,$Opacity:0,$Zoom:0,$Clip:0,$Move:i,$SlideOut:i,$Reverse:i,$Formation:o.$FormationRandom,$Assembly:1032,$ChessMode:{$Column:0,$Row:0},$Easing:d.$EaseSwing,$Round:{},Ob:[],$During:{}};a.p(e,g);e.hc=e.$Cols*e.$Rows;e.$Easing=a.Lc(e.$Easing);e.fe=b.ceil(e.$Duration/e.$Interval);e.ie=function(b,a){b/=e.$Cols;a/=e.$Rows;var f=b+"x"+a;if(!e.Ob[f]){e.Ob[f]={N:b,P:a};for(var c=0;c<e.$Cols;c++)for(var d=0;d<e.$Rows;d++)e.Ob[f][d+","+c]={$Top:d*a,$Right:c*b+b,$Bottom:d*a+a,$Left:c*b}}return e.Ob[f]};if(e.$Brother){e.$Brother=k(e.$Brother,f);e.$SlideOut=c}return e}function p(A,h,d,v,n,l){var y=this,t,u={},j={},m=[],f,e,r,p=d.$ChessMode.$Column||0,q=d.$ChessMode.$Row||0,g=d.ie(n,l),o=B(d),C=o.length-1,s=d.$Duration+d.$Delay*C,w=v+s,k=d.$SlideOut,x;w+=50;function B(a){var b=a.$Formation(a);return a.$Reverse?b.reverse():b}y.td=w;y.Vb=function(c){c-=v;var e=c<s;if(e||x){x=e;if(!k)c=s-c;var f=b.ceil(c/d.$Interval);a.c(j,function(c,e){var d=b.max(f,c.ue);d=b.min(d,c.length-1);if(c.Rc!=d){if(!c.Rc&&!k)a.A(m[e]);else d==c.me&&k&&a.S(m[e]);c.Rc=d;a.Wd(m[e],c[d])}})}};h=a.X(h);if(a.V()){var D=!h["no-image"],z=a.vg(h);a.c(z,function(b){(D||b["jssor-slider"])&&a.Ab(b,a.Ab(b),c)})}a.c(o,function(h,m){a.c(h,function(G){var K=G[0],J=G[1],v=K+","+J,o=i,s=i,x=i;if(p&&J%2){if(p&3)o=!o;if(p&12)s=!s;if(p&16)x=!x}if(q&&K%2){if(q&3)o=!o;if(q&12)s=!s;if(q&16)x=!x}d.$Top=d.$Top||d.$Clip&4;d.$Bottom=d.$Bottom||d.$Clip&8;d.$Left=d.$Left||d.$Clip&1;d.$Right=d.$Right||d.$Clip&2;var C=s?d.$Bottom:d.$Top,z=s?d.$Top:d.$Bottom,B=o?d.$Right:d.$Left,A=o?d.$Left:d.$Right;d.$Clip=C||z||B||A;r={};e={$Top:0,$Left:0,$Opacity:1,N:n,P:l};f=a.p({},e);t=a.p({},g[v]);if(d.$Opacity)e.$Opacity=2-d.$Opacity;if(d.$ZIndex){e.$ZIndex=d.$ZIndex;f.$ZIndex=0}var I=d.$Cols*d.$Rows>1||d.$Clip;if(d.$Zoom||d.$Rotate){var H=c;if(a.V())if(d.$Cols*d.$Rows>1)H=i;else I=i;if(H){e.$Zoom=d.$Zoom?d.$Zoom-1:1;f.$Zoom=1;if(a.V()||a.tc())e.$Zoom=b.min(e.$Zoom,2);var N=d.$Rotate||0;e.$Rotate=N*360*(x?-1:1);f.$Rotate=0}}if(I){var h=t.wb={};if(d.$Clip){var w=d.$ScaleClip||1;if(C&&z){h.$Top=g.P/2*w;h.$Bottom=-h.$Top}else if(C)h.$Bottom=-g.P*w;else if(z)h.$Top=g.P*w;if(B&&A){h.$Left=g.N/2*w;h.$Right=-h.$Left}else if(B)h.$Right=-g.N*w;else if(A)h.$Left=g.N*w}r.$Clip=t;f.$Clip=g[v]}var L=o?1:-1,M=s?1:-1;if(d.x)e.$Left+=n*d.x*L;if(d.y)e.$Top+=l*d.y*M;a.c(e,function(b,c){if(a.Zb(b))if(b!=f[c])r[c]=b-f[c]});u[v]=k?f:e;var D=d.fe,y=b.round(m*d.$Delay/d.$Interval);j[v]=new Array(y);j[v].ue=y;j[v].me=y+D-1;for(var F=0;F<=D;F++){var E=a.Kd(f,r,F/D,d.$Easing,d.$During,d.$Round,{$Move:d.$Move,$OriginalWidth:n,$OriginalHeight:l});E.$ZIndex=E.$ZIndex||1;j[v].push(E)}})});o.reverse();a.c(o,function(b){a.c(b,function(c){var f=c[0],e=c[1],d=f+","+e,b=h;if(e||f)b=a.X(h);a.bb(b,u[d]);a.ib(b,"hidden");a.z(b,"absolute");A.se(b);m[d]=b;a.A(b,!k)})})}function v(){var a=this,b=0;l.call(a,0,u);a.Qb=function(c,a){if(a-b>h){b=a;e&&e.Vb(a);g&&g.Vb(a)}};a.ab=r}f.Ie=function(){var a=0,c=t.$Transitions,d=c.length;if(w)a=x++%d;else a=b.floor(b.random()*d);c[a]&&(c[a].nb=a);return c[a]};f.Ne=function(w,x,j,l,a){r=a;a=k(a,h);var i=l.Wc,d=j.Wc;i["no-image"]=!l.cc;d["no-image"]=!j.cc;var m=i,o=d,v=a,c=a.$Brother||k({},h);if(!a.$SlideOut){m=d;o=i}var t=c.$Shift||0;g=new p(n,o,c,b.max(t-c.$Interval,0),s,q);e=new p(n,m,v,b.max(c.$Interval-t,0),s,q);g.Vb(0);e.Vb(0);u=b.max(g.td,e.td);f.nb=w};f.Db=function(){n.Db();g=j;e=j};f.de=function(){var a=j;if(e)a=new v;return a};if(a.V()||a.tc()||y&&a.ng()<537)h=16;m.call(f);l.call(f,-1e7,1e7)};var h=g.$JssorSlider$=function(q,fc){var o=this;function Ec(){var a=this;l.call(a,-1e8,2e8);a.ge=function(){var c=a.yb(),d=b.floor(c),f=t(d),e=c-b.floor(c);return{nb:f,ce:d,Bb:e}};a.Qb=function(d,a){var e=b.floor(a);if(e!=a&&a>d)e++;Ub(e,c);o.n(h.$EVT_POSITION_CHANGE,t(a),t(d),a,d)}}function Dc(){var b=this;l.call(b,0,0,{Tc:r});a.c(C,function(a){D&1&&a.Ae(r);b.Oc(a);a.$Shift(fb/bc)})}function Cc(){var a=this,b=Tb.$Elmt;l.call(a,-1,2,{$Easing:d.$EaseLinear,xc:{Bb:Zb},Tc:r},b,{Bb:1},{Bb:-2});a.Mb=b}function qc(n,m){var a=this,d,e,g,k,b;l.call(a,-1e8,2e8,{Uc:100});a.Od=function(){O=c;R=j;o.n(h.$EVT_SWIPE_START,t(w.db()),w.db())};a.Ld=function(){O=i;k=i;var a=w.ge();o.n(h.$EVT_SWIPE_END,t(w.db()),w.db());!a.Bb&&Gc(a.ce,s)};a.Qb=function(i,h){var a;if(k)a=b;else{a=e;if(g){var c=h/g;a=f.$SlideEasing(c)*(e-d)+d}}w.v(a)};a.ic=function(b,f,c,h){d=b;e=f;g=c;w.v(b);a.v(0);a.Cd(c,h)};a.je=function(d){k=c;b=d;a.$Play(d,j,c)};a.le=function(a){b=a};w=new Ec;w.I(n);w.I(m)}function rc(){var c=this,b=Xb();a.J(b,0);a.W(b,"pointerEvents","none");c.$Elmt=b;c.se=function(c){a.H(b,c);a.A(b)};c.Db=function(){a.S(b);a.sc(b)}}function Bc(k,e){var d=this,q,H,x,n,y=[],w,B,W,G,Q,F,g,v,p;l.call(d,-u,u+1,{});function E(b){q&&q.jb();T(k,b,0);F=c;q=new I.$Class(k,I,a.Nc(a.j(k,"idle"))||pc);q.v(0)}function Y(){q.rc<I.rc&&E()}function N(p,r,m){if(!G){G=c;if(n&&m){var g=m.width,b=m.height,l=g,k=b;if(g&&b&&f.$FillMode){if(f.$FillMode&3&&(!(f.$FillMode&4)||g>K||b>J)){var j=i,q=K/J*b/g;if(f.$FillMode&1)j=q>1;else if(f.$FillMode&2)j=q<1;l=j?g*J/b:K;k=j?J:b*K/g}a.l(n,l);a.m(n,k);a.G(n,(J-k)/2);a.E(n,(K-l)/2)}a.z(n,"absolute");o.n(h.$EVT_LOAD_END,e)}}a.S(r);p&&p(d)}function X(b,c,f,g){if(g==R&&s==e&&P)if(!Fc){var a=t(b);A.Ne(a,e,c,d,f);c.be();U.$Shift(a-U.Fc()-1);U.v(a);z.ic(b,b,0)}}function ab(b){if(b==R&&s==e){if(!g){var a=j;if(A)if(A.nb==e)a=A.de();else A.Db();Y();g=new yc(k,e,a,q);g.gd(p)}!g.$IsPlaying()&&g.wc()}}function S(c,h,l){if(c==e){if(c!=h)C[h]&&C[h].Be();else!l&&g&&g.Le();p&&p.$Enable();var m=R=a.T();d.Cb(a.K(j,ab,m))}else{var k=b.min(e,c),i=b.max(e,c),o=b.min(i-k,k+r-i),n=u+f.$LazyLoading-1;(!Q||o<=n)&&d.Cb()}}function bb(){if(s==e&&g){g.rb();p&&p.$Quit();p&&p.$Disable();g.nd()}}function db(){s==e&&g&&g.rb()}function Z(a){!M&&o.n(h.$EVT_CLICK,e,a)}function O(){p=v.pInstance;g&&g.gd(p)}d.Cb=function(d,b){b=b||x;if(y.length&&!G){a.A(b);if(!W){W=c;o.n(h.$EVT_LOAD_START,e);a.c(y,function(b){if(!a.C(b,"src")){b.src=a.j(b,"src2");a.Y(b,b["display-origin"])}})}a.Ud(y,n,a.K(j,N,d,b))}else N(d,b)};d.ze=function(){var h=e;if(f.$AutoPlaySteps<0)h-=r;var c=h+f.$AutoPlaySteps*wc;if(D&2)c=t(c);if(!(D&1))c=b.max(0,b.min(c,r-u));if(c!=e){if(A){var d=A.Ie(r);if(d){var i=R=a.T(),g=C[t(c)];return g.Cb(a.K(j,X,c,g,d,i),x)}}nb(c)}};d.pc=function(){S(e,e,c)};d.Be=function(){p&&p.$Quit();p&&p.$Disable();d.od();g&&g.he();g=j;E()};d.be=function(){a.S(k)};d.od=function(){a.A(k)};d.Yd=function(){p&&p.$Enable()};function T(b,d,e){if(a.C(b,"jssor-slider"))return;if(!F){if(b.tagName=="IMG"){y.push(b);if(!a.C(b,"src")){Q=c;b["display-origin"]=a.Y(b);a.S(b)}}a.V()&&a.J(b,(a.J(b)||0)+1)}var f=a.O(b);a.c(f,function(f){var h=f.tagName,j=a.j(f,"u");if(j=="player"&&!v){v=f;if(v.pInstance)O();else a.e(v,"dataavailable",O)}if(j=="caption"){if(d){a.Sc(f,a.j(f,"to"));a.ig(f,a.j(f,"bf"));a.kg(f,"preserve-3d")}else if(!a.Jd()){var g=a.X(f,i,c);a.Pb(g,f,b);a.Hb(f,b);f=g;d=c}}else if(!F&&!e&&!n){if(h=="A"){if(a.j(f,"u")=="image")n=a.xg(f,"IMG");else n=a.D(f,"image",c);if(n){w=f;a.Y(w,"block");a.bb(w,V);B=a.X(w,c);a.z(w,"relative");a.Ab(B,0);a.W(B,"backgroundColor","#000")}}else if(h=="IMG"&&a.j(f,"u")=="image")n=f;if(n){n.border=0;a.bb(n,V)}}T(f,d,e+1)})}d.Ic=function(c,b){var a=u-b;Zb(H,a)};d.nb=e;m.call(d);a.mg(k,a.j(k,"p"));a.lg(k,a.j(k,"po"));var L=a.D(k,"thumb",c);if(L){d.Ee=a.X(L);a.S(L)}a.A(k);x=a.X(cb);a.J(x,1e3);a.e(k,"click",Z);E(c);d.cc=n;d.Ad=B;d.Wc=k;d.Mb=H=k;a.H(H,x);o.$On(203,S);o.$On(28,db);o.$On(24,bb)}function yc(y,f,p,q){var b=this,m=0,u=0,g,j,e,d,k,t,r,n=C[f];l.call(b,0,0);function v(){a.sc(N);dc&&k&&n.Ad&&a.H(N,n.Ad);a.A(N,!k&&n.cc)}function w(){b.wc()}function x(a){r=a;b.rb();b.wc()}b.wc=function(){var a=b.yb();if(!B&&!O&&!r&&s==f){if(!a){if(g&&!k){k=c;b.nd(c);o.n(h.$EVT_SLIDESHOW_START,f,m,u,g,d)}v()}var i,p=h.$EVT_STATE_CHANGE;if(a!=d)if(a==e)i=d;else if(a==j)i=e;else if(!a)i=j;else i=b.Sd();o.n(p,f,a,m,j,e,d);var l=P&&(!E||F);if(a==d)(e!=d&&!(E&12)||l)&&n.ze();else(l||a!=e)&&b.Cd(i,w)}};b.Le=function(){e==d&&e==b.yb()&&b.v(j)};b.he=function(){A&&A.nb==f&&A.Db();var a=b.yb();a<d&&o.n(h.$EVT_STATE_CHANGE,f,-a-1,m,j,e,d)};b.nd=function(b){p&&a.ib(hb,b&&p.ab.$Outside?"":"hidden")};b.Ic=function(b,a){if(k&&a>=g){k=i;v();n.od();A.Db();o.n(h.$EVT_SLIDESHOW_END,f,m,u,g,d)}o.n(h.$EVT_PROGRESS_CHANGE,f,a,m,j,e,d)};b.gd=function(a){if(a&&!t){t=a;a.$On($JssorPlayer$.Ce,x)}};p&&b.Oc(p);g=b.gb();b.Oc(q);j=g+q.dc;e=g+q.Yb;d=b.gb()}function Zb(g,f){var e=x>0?x:gb,c=Bb*f*(e&1),d=Cb*f*(e>>1&1);c=b.round(c);d=b.round(d);a.E(g,c);a.G(g,d)}function Pb(){pb=O;Kb=z.Sd();G=w.db()}function gc(){Pb();if(B||!F&&E&12){z.rb();o.n(h.De)}}function ec(e){if(!B&&(F||!(E&12))&&!z.$IsPlaying()){var c=w.db(),a=b.ceil(G);if(e&&b.abs(H)>=f.$MinDragOffsetToSlide){a=b.ceil(c);a+=eb}if(!(D&1))a=b.min(r-u,b.max(a,0));var d=b.abs(a-c);d=1-b.pow(1-d,5);if(!M&&pb)z.Ke(Kb);else if(c==a){tb.Yd();tb.pc()}else z.ic(c,a,d*Vb)}}function Ib(b){!a.j(a.yc(b),"nodrag")&&a.bc(b)}function uc(a){Yb(a,1)}function Yb(b,d){b=a.vd(b);var k=a.yc(b);if(!L&&!a.j(k,"nodrag")&&vc()&&(!d||b.touches.length==1)){B=c;Ab=i;R=j;a.e(e,d?"touchmove":"mousemove",Db);a.T();M=0;gc();if(!pb)x=0;if(d){var g=b.touches[0];vb=g.clientX;wb=g.clientY}else{var f=a.Qd(b);vb=f.x;wb=f.y}H=0;bb=0;eb=0;o.n(h.$EVT_DRAG_START,t(G),G,b)}}function Db(e){if(B){e=a.vd(e);var f;if(e.type!="mousemove"){var l=e.touches[0];f={x:l.clientX,y:l.clientY}}else f=a.Qd(e);if(f){var j=f.x-vb,k=f.y-wb;if(b.floor(G)!=G)x=x||gb&L;if((j||k)&&!x){if(L==3)if(b.abs(k)>b.abs(j))x=2;else x=1;else x=L;if(jb&&x==1&&b.abs(k)-b.abs(j)>3)Ab=c}if(x){var d=k,i=Cb;if(x==1){d=j;i=Bb}if(!(D&1)){if(d>0){var g=i*s,h=d-g;if(h>0)d=g+b.sqrt(h)*5}if(d<0){var g=i*(r-u-s),h=-d-g;if(h>0)d=-g-b.sqrt(h)*5}}if(H-bb<-2)eb=0;else if(H-bb>2)eb=-1;bb=H;H=d;sb=G-H/i/(Z||1);if(H&&x&&!Ab){a.bc(e);if(!O)z.je(sb);else z.le(sb)}}}}}function mb(){sc();if(B){B=i;a.T();a.R(e,"mousemove",Db);a.R(e,"touchmove",Db);M=H;z.rb();var b=w.db();o.n(h.$EVT_DRAG_END,t(b),b,t(G),G);E&12&&Pb();ec(c)}}function kc(c){if(M){a.Kg(c);var b=a.yc(c);while(b&&v!==b){b.tagName=="A"&&a.bc(c);try{b=b.parentNode}catch(d){break}}}}function oc(a){C[s];s=t(a);tb=C[s];Ub(a);return s}function Gc(a,b){x=0;oc(a);o.n(h.$EVT_PARK,t(a),b)}function Ub(b,c){yb=b;a.c(S,function(a){a.Mc(t(b),b,c)})}function vc(){var b=h.ed||0,a=Y;if(jb)a&1&&(a&=1);h.ed|=a;return L=a&~b}function sc(){if(L){h.ed&=~Y;L=0}}function Xb(){var b=a.mb();a.bb(b,V);a.z(b,"absolute");return b}function t(a){return(a%r+r)%r}function lc(a,c){if(c)if(!D){a=b.min(b.max(a+yb,0),r-u);c=i}else if(D&2){a=t(a+yb);c=i}nb(a,f.$SlideDuration,c)}function zb(){a.c(S,function(a){a.Jc(a.Jb.$ChanceToShow<=F)})}function ic(){if(!F){F=1;zb();if(!B){E&12&&ec();E&3&&C[s].pc()}}}function hc(){if(F){F=0;zb();B||!(E&12)||gc()}}function jc(){V={N:K,P:J,$Top:0,$Left:0};a.c(T,function(b){a.bb(b,V);a.z(b,"absolute");a.ib(b,"hidden");a.S(b)});a.bb(cb,V)}function lb(b,a){nb(b,a,c)}function nb(g,e,l){if(Rb&&(!B&&(F||!(E&12))||f.$NaviQuitDrag)){O=c;B=i;z.rb();if(e==k)e=Vb;var d=Eb.yb(),a=g;if(l){a=d+g;if(g>0)a=b.ceil(a);else a=b.floor(a)}if(D&2)a=t(a);if(!(D&1))a=b.max(0,b.min(a,r-u));var j=(a-d)%r;a=d+j;var h=d==a?0:e*b.abs(j);h=b.min(h,e*u*1.5);z.ic(d,a,h||1)}}o.$PlayTo=nb;o.$GoTo=function(a){w.v(a)};o.$Next=function(){lb(1)};o.$Prev=function(){lb(-1)};o.$Pause=function(){P=i};o.$Play=function(){if(!P){P=c;C[s]&&C[s].pc()}};o.$SetSlideshowTransitions=function(a){f.$SlideshowOptions.$Transitions=a};o.$SetCaptionTransitions=function(b){I.$Transitions=b;I.rc=a.T()};o.$SlidesCount=function(){return T.length};o.$CurrentIndex=function(){return s};o.$IsAutoPlaying=function(){return P};o.$IsDragging=function(){return B};o.$IsSliding=function(){return O};o.$IsMouseOver=function(){return!F};o.$LastDragSucceded=function(){return M};function X(){return a.l(y||q)}function ib(){return a.m(y||q)}o.$OriginalWidth=o.$GetOriginalWidth=X;o.$OriginalHeight=o.$GetOriginalHeight=ib;function Gb(c,d){if(c==k)return a.l(q);if(!y){var b=a.mb(e);a.Zc(b,a.Zc(q));a.Nb(b,a.Nb(q));a.Y(b,"block");a.z(b,"relative");a.G(b,0);a.E(b,0);a.ib(b,"visible");y=a.mb(e);a.z(y,"absolute");a.G(y,0);a.E(y,0);a.l(y,a.l(q));a.m(y,a.m(q));a.Sc(y,"0 0");a.H(y,b);var h=a.O(q);a.H(q,y);a.W(q,"backgroundImage","");a.c(h,function(c){a.H(a.j(c,"noscale")?q:b,c);a.j(c,"autocenter")&&Lb.push(c)})}Z=c/(d?a.m:a.l)(y);a.sg(y,Z);var g=d?Z*X():c,f=d?c:Z*ib();a.l(q,g);a.m(q,f);a.c(Lb,function(b){var c=a.Kb(a.j(b,"autocenter"));a.Oe(b,c)})}o.$ScaleHeight=o.$GetScaleHeight=function(b){if(b==k)return a.m(q);Gb(b,c)};o.$ScaleWidth=o.$SetScaleWidth=o.$GetScaleWidth=Gb;o.Ed=function(a){var d=b.ceil(t(fb/bc)),c=t(a-s+d);if(c>u){if(a-s>r/2)a-=r;else if(a-s<=-r/2)a+=r}else a=s+c-d;return a};m.call(o);o.$Elmt=q=a.qb(q);var f=a.p({$FillMode:0,$LazyLoading:1,$ArrowKeyNavigation:1,$StartIndex:0,$AutoPlay:i,$Loop:1,$NaviQuitDrag:c,$AutoPlaySteps:1,$AutoPlayInterval:3e3,$PauseOnHover:1,$SlideDuration:500,$SlideEasing:d.$EaseOutQuad,$MinDragOffsetToSlide:20,$SlideSpacing:0,$Cols:1,$Align:0,$UISearchMode:1,$PlayOrientation:1,$DragOrientation:1},fc);if(f.$Idle!=k)f.$AutoPlayInterval=f.$Idle;if(f.$DisplayPieces!=k)f.$Cols=f.$DisplayPieces;if(f.$ParkingPosition!=k)f.$Align=f.$ParkingPosition;var gb=f.$PlayOrientation&3,wc=(f.$PlayOrientation&4)/-4||1,db=f.$SlideshowOptions,I=a.p({$Class:p,$PlayInMode:1,$PlayOutMode:1},f.$CaptionSliderOptions);I.$Transitions=I.$Transitions||I.$CaptionTransitions;var qb=f.$BulletNavigatorOptions,W=f.$ArrowNavigatorOptions,ab=f.$ThumbnailNavigatorOptions,Q=!f.$UISearchMode,y,v=a.D(q,"slides",Q),cb=a.D(q,"loading",Q)||a.mb(e),Jb=a.D(q,"navigator",Q),cc=a.D(q,"arrowleft",Q),ac=a.D(q,"arrowright",Q),Hb=a.D(q,"thumbnavigator",Q),nc=a.l(v),mc=a.m(v),V,T=[],xc=a.O(v);a.c(xc,function(b){if(b.tagName=="DIV"&&!a.j(b,"u"))T.push(b);else a.V()&&a.J(b,(a.J(b)||0)+1)});var s=-1,yb,tb,r=T.length,K=f.$SlideWidth||nc,J=f.$SlideHeight||mc,Wb=f.$SlideSpacing,Bb=K+Wb,Cb=J+Wb,bc=gb&1?Bb:Cb,u=b.min(f.$Cols,r),hb,x,L,Ab,S=[],Qb,Sb,Ob,dc,Fc,P,E=f.$PauseOnHover,pc=f.$AutoPlayInterval,Vb=f.$SlideDuration,rb,ub,fb,Rb=u<r,D=Rb?f.$Loop:0,Y,M,F=1,O,B,R,vb=0,wb=0,H,bb,eb,Eb,w,U,z,Tb=new rc,Z,Lb=[];P=f.$AutoPlay;o.Jb=fc;jc();a.C(q,"jssor-slider",c);a.J(v,a.J(v)||0);a.z(v,"absolute");hb=a.X(v,c);a.Pb(hb,v);if(db){dc=db.$ShowLink;rb=db.$Class;ub=u==1&&r>1&&rb&&(!a.Jd()||a.sd()>=8)}fb=ub||u>=r||!(D&1)?0:f.$Align;Y=(u>1||fb?gb:-1)&f.$DragOrientation;var xb=v,C=[],A,N,Fb=a.Pg(),jb=Fb.Rg,G,pb,Kb,sb;Fb.Bd&&a.W(xb,Fb.Bd,([j,"pan-y","pan-x","none"])[Y]||"");U=new Cc;if(ub)A=new rb(Tb,K,J,db,jb);a.H(hb,U.Mb);a.ib(v,"hidden");N=Xb();a.W(N,"backgroundColor","#000");a.Ab(N,0);a.Pb(N,xb.firstChild,xb);for(var ob=0;ob<T.length;ob++){var zc=T[ob],Ac=new Bc(zc,ob);C.push(Ac)}a.S(cb);Eb=new Dc;z=new qc(Eb,U);if(Y){a.e(v,"mousedown",Yb);a.e(v,"touchstart",uc);a.e(v,"dragstart",Ib);a.e(v,"selectstart",Ib);a.e(e,"mouseup",mb);a.e(e,"touchend",mb);a.e(e,"touchcancel",mb);a.e(g,"blur",mb)}E&=jb?10:5;if(Jb&&qb){Qb=new qb.$Class(Jb,qb,X(),ib());S.push(Qb)}if(W&&cc&&ac){W.$Loop=D;W.$Cols=u;Sb=new W.$Class(cc,ac,W,X(),ib());S.push(Sb)}if(Hb&&ab){ab.$StartIndex=f.$StartIndex;Ob=new ab.$Class(Hb,ab);S.push(Ob)}a.c(S,function(a){a.Gc(r,C,cb);a.$On(n.fc,lc)});a.W(q,"visibility","visible");Gb(X());a.e(v,"click",kc);a.e(q,"mouseout",a.Ib(ic,q));a.e(q,"mouseover",a.Ib(hc,q));zb();f.$ArrowKeyNavigation&&a.e(e,"keydown",function(a){if(a.keyCode==37)lb(-f.$ArrowKeyNavigation);else a.keyCode==39&&lb(f.$ArrowKeyNavigation)});var kb=f.$StartIndex;if(!(D&1))kb=b.max(0,b.min(kb,r-u));z.ic(kb,kb,0)};h.$EVT_CLICK=21;h.$EVT_DRAG_START=22;h.$EVT_DRAG_END=23;h.$EVT_SWIPE_START=24;h.$EVT_SWIPE_END=25;h.$EVT_LOAD_START=26;h.$EVT_LOAD_END=27;h.De=28;h.$EVT_POSITION_CHANGE=202;h.$EVT_PARK=203;h.$EVT_SLIDESHOW_START=206;h.$EVT_SLIDESHOW_END=207;h.$EVT_PROGRESS_CHANGE=208;h.$EVT_STATE_CHANGE=209;var n={fc:1};g.$JssorBulletNavigator$=function(e,C){var f=this;m.call(f);e=a.qb(e);var s,A,z,r,l=0,d,o,k,w,x,h,g,q,p,B=[],y=[];function v(a){a!=-1&&y[a].jd(a==l)}function t(a){f.n(n.fc,a*o)}f.$Elmt=e;f.Mc=function(a){if(a!=r){var d=l,c=b.floor(a/o);l=c;r=a;v(d);v(c)}};f.Jc=function(b){a.A(e,b)};var u;f.Gc=function(E){if(!u){s=b.ceil(E/o);l=0;var n=q+w,r=p+x,m=b.ceil(s/k)-1;A=q+n*(!h?m:k-1);z=p+r*(h?m:k-1);a.l(e,A);a.m(e,z);for(var f=0;f<s;f++){var C=a.Cg();a.ug(C,f+1);var i=a.Xc(g,"numbertemplate",C,c);a.z(i,"absolute");var v=f%(m+1);a.E(i,!h?n*v:f%k*n);a.G(i,h?r*v:b.floor(f/(m+1))*r);a.H(e,i);B[f]=i;d.$ActionMode&1&&a.e(i,"click",a.K(j,t,f));d.$ActionMode&2&&a.e(i,"mouseover",a.Ib(a.K(j,t,f),i));y[f]=a.ac(i)}u=c}};f.Jb=d=a.p({$SpacingX:10,$SpacingY:10,$Orientation:1,$ActionMode:1},C);g=a.D(e,"prototype");q=a.l(g);p=a.m(g);a.Hb(g,e);o=d.$Steps||1;k=d.$Lanes||1;w=d.$SpacingX;x=d.$SpacingY;h=d.$Orientation-1;d.$Scale==i&&a.C(e,"noscale",c);d.$AutoCenter&&a.C(e,"autocenter",d.$AutoCenter)};g.$JssorArrowNavigator$=function(b,g,h){var d=this;m.call(d);var r,q,e,f,k;a.l(b);a.m(b);function l(a){d.n(n.fc,a,c)}function p(c){a.A(b,c||!h.$Loop&&e==0);a.A(g,c||!h.$Loop&&e>=q-h.$Cols);r=c}d.Mc=function(b,a,c){if(c)e=a;else{e=b;p(r)}};d.Jc=p;var o;d.Gc=function(d){q=d;e=0;if(!o){a.e(b,"click",a.K(j,l,-k));a.e(g,"click",a.K(j,l,k));a.ac(b);a.ac(g);o=c}};d.Jb=f=a.p({$Steps:1},h);k=f.$Steps;if(f.$Scale==i){a.C(b,"noscale",c);a.C(g,"noscale",c)}if(f.$AutoCenter){a.C(b,"autocenter",f.$AutoCenter);a.C(g,"autocenter",f.$AutoCenter)}};g.$JssorThumbnailNavigator$=function(g,C){var l=this,z,q,d,w=[],A,y,e,r,s,v,u,p,t,f,o;m.call(l);g=a.qb(g);function B(m,f){var g=this,b,k,i;function p(){k.jd(q==f)}function h(d){if(d||!t.$LastDragSucceded()){var a=e-f%e,b=t.Ed((f+a)/e-1),c=b*e+e-a;l.n(n.fc,c)}}g.nb=f;g.bd=p;i=m.Ee||m.cc||a.mb();g.Mb=b=a.Xc(o,"thumbnailtemplate",i,c);k=a.ac(b);d.$ActionMode&1&&a.e(b,"click",a.K(j,h,0));d.$ActionMode&2&&a.e(b,"mouseover",a.Ib(a.K(j,h,1),b))}l.Mc=function(c,d,f){var a=q;q=c;a!=-1&&w[a].bd();w[c].bd();!f&&t.$PlayTo(t.Ed(b.floor(d/e)))};l.Jc=function(b){a.A(g,b)};var x;l.Gc=function(F,C){if(!x){z=F;b.ceil(z/e);q=-1;p=b.min(p,C.length);var j=d.$Orientation&1,m=v+(v+r)*(e-1)*(1-j),l=u+(u+s)*(e-1)*j,o=m+(m+r)*(p-1)*j,n=l+(l+s)*(p-1)*(1-j);a.z(f,"absolute");a.ib(f,"hidden");d.$AutoCenter&1&&a.E(f,(A-o)/2);d.$AutoCenter&2&&a.G(f,(y-n)/2);a.l(f,o);a.m(f,n);var k=[];a.c(C,function(l,g){var h=new B(l,g),d=h.Mb,c=b.floor(g/e),i=g%e;a.E(d,(v+r)*i*(1-j));a.G(d,(u+s)*i*j);if(!k[c]){k[c]=a.mb();a.H(f,k[c])}a.H(k[c],d);w.push(h)});var E=a.p({$AutoPlay:i,$NaviQuitDrag:i,$SlideWidth:m,$SlideHeight:l,$SlideSpacing:r*j+s*(1-j),$MinDragOffsetToSlide:12,$SlideDuration:200,$PauseOnHover:1,$PlayOrientation:d.$Orientation,$DragOrientation:d.$NoDrag||d.$DisableDrag?0:d.$Orientation},d);t=new h(g,E);x=c}};l.Jb=d=a.p({$SpacingX:0,$SpacingY:0,$Cols:1,$Orientation:1,$AutoCenter:3,$ActionMode:1},C);if(d.$DisplayPieces!=k)d.$Cols=d.$DisplayPieces;if(d.$Rows!=k)d.$Lanes=d.$Rows;A=a.l(g);y=a.m(g);f=a.D(g,"slides",c);o=a.D(f,"prototype");v=a.l(o);u=a.m(o);a.Hb(o,f);e=d.$Lanes||1;r=d.$SpacingX;s=d.$SpacingY;p=d.$Cols;d.$Scale==i&&a.C(g,"noscale",c)};function p(e,d,c){var b=this;l.call(b,0,c);b.jb=a.kd;b.dc=0;b.Yb=c}g.$JssorCaptionSlider$=function(h,f,i){var c=this;l.call(c,0,0);var e,d;function g(p,h,f){var c=this,g,n=f?h.$PlayInMode:h.$PlayOutMode,e=h.$Transitions,o={ab:"t",$Delay:"d",$Duration:"du",x:"x",y:"y",$Rotate:"r",$Zoom:"z",$Opacity:"f",Gb:"b"},d={kb:function(b,a){if(!isNaN(a.sb))b=a.sb;else b*=a.Kf;return b},$Opacity:function(b,a){return this.kb(b-1,a)}};d.$Zoom=d.$Opacity;l.call(c,0,0);function j(r,m){var l=[],i,k=[],c=[];function h(c,d){var b={};a.c(o,function(g,h){var e=a.j(c,g+(d||""));if(e){var f={};if(g=="t")f.sb=e;else if(e.indexOf("%")+1)f.Kf=a.Nc(e)/100;else f.sb=a.Nc(e);b[h]=f}});return b}function p(){return e[b.floor(b.random()*e.length)]}function g(f){var h;if(f=="*")h=p();else if(f){var d=e[a.Kb(f)]||e[f];if(a.uc(d)){if(f!=i){i=f;c[f]=0;k[f]=d[b.floor(b.random()*d.length)]}else c[f]++;d=k[f];if(a.uc(d)){d=d.length&&d[c[f]%d.length];if(a.uc(d))d=d[b.floor(b.random()*d.length)]}}h=d;if(a.ud(h))h=g(h)}return h}var q=a.O(r);a.c(q,function(b){var c=[];c.$Elmt=b;var e=a.j(b,"u")=="caption";a.c(f?[0,3]:[2],function(l,o){if(e){var k,f;if(l!=2||!a.j(b,"t3")){f=h(b,l);if(l==2&&!f.ab){f.$Delay=f.$Delay||{sb:0};f=a.p(h(b,0),f)}}if(f&&f.ab){k=g(f.ab.sb);if(k){var i=a.p({$Delay:0},k);a.c(f,function(c,a){var b=(d[a]||d.kb).apply(d,[i[a],f[a]]);if(!isNaN(b))i[a]=b});if(!o)if(f.Gb)i.Gb=f.Gb.sb||0;else if(n&2)i.Gb=0}}c.push(i)}if(m%2&&!o)c.O=j(b,m+1)});l.push(c)});return l}function m(w,c,z){var g={$Easing:c.$Easing,$Round:c.$Round,$During:c.$During,$Reverse:f&&!z},m=w,r=a.Yc(w),k=a.l(m),j=a.m(m),y=a.l(r),x=a.m(r),h={},e={},i=c.$ScaleClip||1;if(c.$Opacity)e.$Opacity=1-c.$Opacity;g.$OriginalWidth=k;g.$OriginalHeight=j;if(c.$Zoom||c.$Rotate){e.$Zoom=(c.$Zoom||2)-2;if(a.V()||a.tc())e.$Zoom=b.min(e.$Zoom,1);h.$Zoom=1;var B=c.$Rotate||0;e.$Rotate=B*360;h.$Rotate=0}else if(c.$Clip){var s={$Top:0,$Right:k,$Bottom:j,$Left:0},v=a.p({},s),d=v.wb={},u=c.$Clip&4,p=c.$Clip&8,t=c.$Clip&1,q=c.$Clip&2;if(u&&p){d.$Top=j/2*i;d.$Bottom=-d.$Top}else if(u)d.$Bottom=-j*i;else if(p)d.$Top=j*i;if(t&&q){d.$Left=k/2*i;d.$Right=-d.$Left}else if(t)d.$Right=-k*i;else if(q)d.$Left=k*i;g.$Move=c.$Move;e.$Clip=v;h.$Clip=s}var n=0,o=0;if(c.x)n-=y*c.x;if(c.y)o-=x*c.y;if(n||o||g.$Move){e.$Left=n;e.$Top=o}var A=c.$Duration;h=a.p(h,a.xe(m,e));g.xc=a.Pc();return new l(c.$Delay,A,g,m,h,e)}function i(b,d){a.c(d,function(d){var a,h=d.$Elmt,e=d[0],j=d[1];if(e){a=m(h,e);e.Gb==k&&a.$Shift(b);b=a.gb()}b=i(b,d.O);if(j){var f=m(h,j,1);f.$Shift(b);c.I(f);g.I(f)}a&&c.I(a)});return b}c.jb=function(){c.v(c.gb()*(f||0));g.v(0)};g=new l(0,0);i(0,n?j(p,1):[])}c.jb=function(){d.jb();e.jb()};e=new g(h,f,1);c.dc=e.gb();c.Yb=c.dc+i;d=new g(h,f);d.$Shift(c.Yb);c.I(d);c.I(e)};g.$JssorCaptionSlideo$=function(n,g,m){var b=this,o,h={},i=g.$Transitions,d=new l(0,0);l.call(b,0,0);function j(d,c){var b={};a.c(d,function(d,f){var e=h[f];if(e){if(a.yg(d))d=j(d,c||f=="e");else if(c)if(a.Zb(d))d=o[d];b[e]=d}});return b}function k(e,c){var b=[],d=a.O(e);a.c(d,function(d){var h=a.j(d,"u")=="caption";if(h){var e=a.j(d,"t"),g=i[a.Kb(e)]||i[e],f={$Elmt:d,ab:g};b.push(f)}if(c<5)b=b.concat(k(d,c+1))});return b}function r(c,e,b){a.c(e,function(f){var e=j(f),g={$Easing:a.Lc(e.$Easing),xc:a.Pc(),$OriginalWidth:b.N,$OriginalHeight:b.P},h=new l(f.b,f.d,g,c,b,e);d.I(h);b=a.Je(b,e)});return b}function q(b){a.c(b,function(e){var b=e.$Elmt,d=a.l(b),c=a.m(b),f={$Left:a.E(b),$Top:a.G(b),$Opacity:1,$ZIndex:a.J(b)||0,$Rotate:0,$RotateX:0,$RotateY:0,$ScaleX:1,$ScaleY:1,$TranslateX:0,$TranslateY:0,$TranslateZ:0,$SkewX:0,$SkewY:0,N:d,P:c,$Clip:{$Top:0,$Right:d,$Bottom:c,$Left:0}};r(b,e.ab,f)})}function t(g,f,h){var e=g.b-f;if(e){var a=new l(f,e);a.I(d,c);a.$Shift(h);b.I(a)}b.ye(g.d);return e}function s(f){var c=d.Fc(),e=0;a.c(f,function(d,f){d=a.p({d:m},d);t(d,c,e);c=d.b;e+=d.d;if(!f||d.t==2){b.dc=c;b.Yb=c+d.d}})}b.jb=function(){b.v(-1,c)};o=[f.$Swing,f.$Linear,f.$InQuad,f.$OutQuad,f.$InOutQuad,f.$InCubic,f.$OutCubic,f.$InOutCubic,f.$InQuart,f.$OutQuart,f.$InOutQuart,f.$InQuint,f.$OutQuint,f.$InOutQuint,f.$InSine,f.$OutSine,f.$InOutSine,f.$InExpo,f.$OutExpo,f.$InOutExpo,f.$InCirc,f.$OutCirc,f.$InOutCirc,f.$InElastic,f.$OutElastic,f.$InOutElastic,f.$InBack,f.$OutBack,f.$InOutBack,f.$InBounce,f.$OutBounce,f.$InOutBounce,f.$GoBack,f.$InWave,f.$OutWave,f.$OutJump,f.$InJump];var u={$Top:"y",$Left:"x",$Bottom:"m",$Right:"t",$Rotate:"r",$RotateX:"rX",$RotateY:"rY",$ScaleX:"sX",$ScaleY:"sY",$TranslateX:"tX",$TranslateY:"tY",$TranslateZ:"tZ",$SkewX:"kX",$SkewY:"kY",$Opacity:"o",$Easing:"e",$ZIndex:"i",$Clip:"c"};a.c(u,function(b,a){h[b]=a});q(k(n,1));d.v(-1);var p=g.$Breaks||[],e=[].concat(p[a.Kb(a.j(n,"b"))]||[]);e.push({b:d.gb(),d:e.length?0:m});s(e);b.v(-1)}})(window,document,Math,null,true,false)
jQuery(document).ready(function ($) {
            
            var jssor_1_options = {
              $AutoPlay: true,
              $Idle: 0,
              $AutoPlaySteps: 4,
              $SlideDuration: 1600,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 140,
              $Cols: 8
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 809);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end





            // Tabs Navigation
            $('.tab-center:not(:first)').hide();
            $('.tab-center:first').show();
            $('.client-tabs li a').click(function(){
              $('.client-tabs li a').removeClass('active');
              $(this).addClass('active');
              $('.tab-center').hide();
              var activeTab = $(this).attr('href');
              $(activeTab).fadeIn(500);
              return false;
            });


  var showChar = 500;
  var ellipsestext = "...";
  var moretext = "Read More >>";
  var lesstext = "<< Less";
  $('.read-more-text').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      $(this).html(html);
    }

  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });


        });