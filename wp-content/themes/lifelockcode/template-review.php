<?php
/*
Template Name: Reviews
 */

use Symfony\Component\DomCrawler\Crawler;
require_once __DIR__ . '/vendor/autoload.php';
?>
<?php
$get_url = str_replace('', '', $_SERVER['REQUEST_URI']);
$get_url = explode("?", $get_url);
$begin_get_var = 0;
$url_child = $get_url[$begin_get_var + 1];
$url_main = 'https://www.lifelock.com/reviews/' . $url_child;

$html = file_get_contents('https://www.lifelock.com/reviews/');
$crawler = new Crawler($html);

try {
  $totalReviews = $crawler->filter('.numReviews > div')->text();
  $avgReviews = $crawler->filter('.avgReviews > div')->text();
  $starsReviews = $crawler->filter('article.stats > section.col-sm-5.hidden-xs.stars')->html();
} catch (Exception $e) {}

$links = [
  ['url' => $url_main],
];
$app = new App;

if (has_post_thumbnail()) {
	$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
	$url = $image[0];
} else {
	$url = '';
}
get_header();
?>
<style type="text/css">
  #page-reviews .cover-img {
  background-image: url(<?php echo $url; ?>);
  background-size: 100% 100%;
  height: 450px;
  margin: 0 auto;
  width: 1200px;
}
</style>
<div id="page-reviews" class="box-shadow">
  <?php while (have_posts()): the_post();?>
        <?php 
          $id_page = get_the_ID();
          $id_code = '134';
          $get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
          while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
            $id_code = get_the_ID();
          endwhile;

          $arr_infoproduct = [];
          $info_products = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1,) ); 
            while($info_products->have_posts() ) : 
                $info_products->the_post();
                $arr_infoproduct[get_the_title()]['id'] = get_the_ID();
                $arr_infoproduct[get_the_title()]['option'] = get_field('option', get_the_ID());
            endwhile;
          $max_discount = get_post_meta($id_code, 'discount', true);
          $max_term = get_post_meta($id_code, 'term', true);
        ?>
			    <div class="cover-img">
			        <div class="cover-text">
			            <h2><?php the_title();?></h2>
			            <p><?php the_content(); ?></p>
			            <a href="<?php echo render_url($id_code, $id_page); ?>" target="_blank" title="Promo Code: LLC3015 | Save 15% + 30 Days Risk Free* | ENROLL NOW">Promo Code: <?php the_field('code',$id_code); ?> | Save <?php echo $max_discount; ?>% + <?php echo $max_term; ?> Days Risk Free* | ENROLL NOW</a>
			            <div class="icon-togger">
			                <div class="point"></div>
			            </div>
			        </div>
			    </div>
			    <div class="content-reviews">
			        <p class="description" style=""><?php the_content();?></p>
			        <div class="client-tabs ">
			            <ul>
			                <li ><a class="" href="#tabs-1" title="Member Reviews">Member Reviews</a></li>
			                <li ><a href="#tabs-2" title="Website Reviews" class="active">Website Reviews</a></li>
			            </ul>
			        </div>
			        <div id="tabs-1" class="client-readmore tab-center" style="display: none;">
			            <div class="control-box-content">
			                <div class="control-the-star">
			                    <div class="title">
			                        <ul class="parent">
			                            <li class="title-child">
			                                <span>View</span>
			                                <ul>
			                                    <li><a href="<?php echo site_url(); ?>/reviews/" title="All Star Rating">All Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?5-star" title="5 Star Rating">5 Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?4-star" title="4 Star Rating">4 Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?3-star" title="3 Star Rating">3 Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?2-star" title="2 Star Rating">2 Star Rating</a></li>
			                                    <li><a href="<?php echo site_url(); ?>/reviews?1-star" title="1 Star Rating">1 Star Rating</a></li>
			                                </ul>
			                            </li>
			                        </ul>
			                    </div>
			                </div>
			                <ul class="control-the-content">
			                    <li><a class="active" href="<?php echo site_url(); ?>/reviews/" title="All">All</a></li>
			                    <li><a href="<?php echo site_url(); ?>/reviews?lifelock-standard" title="Lifelock Standard">Lifelock Standard</a></li>
			                    <li><a href="<?php echo site_url(); ?>/reviews?lifelock-advantage" title="Lifelock Advantage">Lifelock Advantage</a></li>
			                    <li><a href="<?php echo site_url(); ?>/reviews?lifelock-ultimate-plus" title="Lifelock Ultimate Plus">Lifelock Ultimate Plus</a></li>
			                </ul>
			            </div>
			            <div class="box-reviews-star">
			                <div class="total-review">
			                    <h2><?php echo $totalReviews; ?></h2>
			                    <h3>REVIEWS</h3>
			                </div>
			                <div class="average-review">
			                    <h2><?php echo $avgReviews; ?></h2>
			                    <h3>AVERAGE REVIEWS</h3>
			                </div>
			                <div class="chart-review">
			                    <?php echo $starsReviews ?>
			                </div>
			            </div>
			            <p class="mind-10-reviews"><span>*</span>Showing the most recent 10 reviews</p>
			            <ul>
                    <?php      
                      foreach ($links as &$link) {
                          $link = $app->getData($link['url']);
                          foreach ($link as $value) {
                              $title = $value['title'];
                              $str = $value['str'];
                              $reviewContent = $value['reviewDetail'];
                              $star = $value['star'];
                               ?>
                              <li class="client">
                                  <div class="client-comment">
                                      <h4><?php echo $title; ?></h4>
                                      <p class="previews-calendar">
                                          <div class="box-left-star">
                                              <div class="star-reviews"><span class="<?php echo $star; ?>">&nbsp</span></div>
                                              <span><i class="fa fa-calendar"></i> <?php echo $reviewContent; ?></span>
                                          </div>
                                      </p>
                                      <p class="description comment read-more-text">
                                          <?php echo $str; ?>
                                      </p>
                                  </div>
                              </li>
                              <hr class="space-box" />
                              <?php 
                          }
                      }
                    ?>
			            </ul>
			        </div>
			        <div id="tabs-2" class="client-readmore tab-center" style="display: block;">
			          <!-- ==================== use Jssor js ============================== -->
			            <div id="jssor_1" style="display: none; position: relative; margin: 0px auto; top: 0px; left: 0px; width: 809px; height: 82.551px; overflow: hidden; visibility: visible;" jssor-slider="true">

			            <div style="position: absolute; top: 0px; left: 0px; width: 980px; height: 100px; transform-origin: 0px 0px 0px; transform: scale(0.82551);"><div class="" style="display: block; position: relative; margin: 0px auto; top: 0px; left: 0px; width: 980px; height: 100px; overflow: visible; visibility: visible;"><div data-u="slides" style="cursor: default; position: absolute; top: 0px; left: 0px; width: 980px; height: 100px; overflow: hidden; z-index: 0;"><div style="position: absolute; z-index: 0; pointer-events: none; left: 0px; top: 0px;"></div></div><div data-u="slides" style="cursor: default; position: absolute; top: 0px; left: 0px; width: 980px; height: 100px; overflow: hidden; z-index: 0;"><div style="width: 140px; height: 100px; top: 0px; left: 0px; position: absolute; background-color: rgb(0, 0, 0); opacity: 0;"></div>
			                </div></div></div></div>
			            <!-- ===================================================================== -->
			            <ul>
                    <li class="client">
                        <div class="client-comment">
                            <h4>PCMAG</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.pcmag.com/article2/0%2c2817%2c2478669%2c00.asp"  target="_blank" >http://www.pcmag.com/article2/0%2c2817%2c2478669%2c00.asp</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Tom’s Guide</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.tomsguide.com/us/lifelock-ultimate-plus,review-2807.html"  target="_blank" >http://www.tomsguide.com/us/lifelock-ultimate-plus,review-2807.html</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Next Advisor</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.nextadvisor.com/identity_theft_protection_services/lifelock_ultimate_plus_review.php"  target="_blank" >http://www.nextadvisor.com/identity_theft_protection_services/lifelock_ultimate_plus_review.php</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Theft Authority</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.idtheftauthority.com/reviews/lifelock/"  target="_blank" >http://www.idtheftauthority.com/reviews/lifelock/</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Elite Personal Finance</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.elitepersonalfinance.com/lifelock/"  target="_blank" >http://www.elitepersonalfinance.com/lifelock/</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Linkedin</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="https://www.linkedin.com/pulse/lifelock-reviews-news-report-now-published-quinn-william"  target="_blank" >https://www.linkedin.com/pulse/lifelock-reviews-news-report-now-published-quinn-william</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Yahoo! Finance</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://finance.yahoo.com/news/lifelock-review-scam-success-065800563.html"  target="_blank" >http://finance.yahoo.com/news/lifelock-review-scam-success-065800563.html</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Super Money</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="https://www.supermoney.com/reviews/credit-reporting/lifelock"  target="_blank" >https://www.supermoney.com/reviews/credit-reporting/lifelock</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>Identity Theft Labs</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="http://www.identitytheftlabs.com/lifelock-review/"  target="_blank" >http://www.identitytheftlabs.com/lifelock-review/</a>
                            </p>
                        </div>
                    </li>
                    <li class="client">
                        <div class="client-comment">
                            <h4>In Home Safety Guide</h4>
                            <p class="previews-calendar">
                                <span><img src="../templates/requestlifelocktheme/images/page_template/client-vote.jpg"></span>
                                <span><i class="fa fa-calendar"></i> December 19, 2015</span>
                            </p>
                            <p class="description comment read-more-text">
                                <a href="https://www.inhomesafetyguide.org/lifelock-review/"  target="_blank" >https://www.inhomesafetyguide.org/lifelock-review/</a>
                            </p>
                        </div>
                    </li>
                </ul>   
			        </div>
			    </div>
			  <?php endwhile;?>
</div>
<?php get_footer();?>