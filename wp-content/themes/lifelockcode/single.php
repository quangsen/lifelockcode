<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="warning">
<?php the_field('warning','13'); ?>
</div>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();
			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
			$post_type = get_post_type();
			if($post_type == 'landing_page') {
				get_template_part( 'single-landingpage', get_post_format() );
			} else {
				get_template_part( 'content-blog', get_post_format() );
			}

			// If comments are open or we have at least one comment, load up the comment template.
			//if ( comments_open() || get_comments_number() ) :
			//	comments_template();
			//endif;

			// Previous/next post navigation.
			//the_post_navigation( array(
			//	'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'lifelockcode' ) . '</span> ' .
			//		'<span class="screen-reader-text">' . __( 'Next post:', 'lifelockcode' ) . '</span> ' .
			//		'<span class="post-title">%title</span>',
			//	'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'lifelockcode' ) . '</span> ' .
			//		'<span class="screen-reader-text">' . __( 'Previous post:', 'lifelockcode' ) . '</span> ' .
				//	'<span class="post-title">%title</span>',
			//) );

		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
