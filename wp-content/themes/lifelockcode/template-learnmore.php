<?php
/*
Template Name: Learn More
 */
get_header();
?>
<div id="main-wrapper">
	<div class="pagesCont">
	<?php while( have_posts() ) : the_post(); ?>
		<div id="system-message-container"></div>
			<div class="item-page" itemscope="" itemtype="http://schema.org/Article">
				<meta itemprop="inLanguage" content="en-GB">
				<div itemprop="articleBody">
					<div class="innerCont w1354">
						<div class="largeTitle">
							<h1>Identity Theft</h1>
						</div>
						<div class="box-shadow">
							<h2><?php echo get_the_title(); ?></h2>
							<img style="margin: 5px; float: left;" title="Identity Theft" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="LifeLock coupon" width="161" height="139">
							<?php echo the_content(); ?>
						</div>
					</div> 	
				</div>
			</div>
		<div class="sidebar-landingpage homeTopLeft fr re_fl"></div>
	<?php endwhile; ?>
	</div>
</div>
<?php get_footer();?>