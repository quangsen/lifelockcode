<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since lifelockcode 1.0
 */
?>

	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="footercontainer">
		<div class="footerTop justify contaicner">
			<div class="col-md-3">
				<?php dynamic_sidebar( 'footer-1' ); ?>
			</div>
			<div class="col-md-3">
				<?php dynamic_sidebar( 'footer-2' ); ?>
			</div>
			<div class="col-md-3">
				<?php dynamic_sidebar( 'footer-3' ); ?>
			</div>
			<div class="col-md-3">
				<?php dynamic_sidebar( 'footer-4' ); ?>
			</div>
		
		</div>
		<div class="site-info footerBot">
		<?php dynamic_sidebar( 'footer-bot' ); ?>
		</div><!-- .site-info -->
		</div>
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
