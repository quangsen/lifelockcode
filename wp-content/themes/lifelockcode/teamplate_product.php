<?php
/*
Template Name: Products
*/
get_header(); ?>

<?php while ( have_posts() ) : the_post(); $title = get_the_title(); ?>
	<?php 
		$id_page = get_the_ID();
		$id_code = '134';
		$get_post_code = new WP_Query( array( 'post_type' => 'Code','posts_per_page'=> 1,) );
		while ( $get_post_code->have_posts() ) : $get_post_code->the_post();
			$id_code = get_the_ID();
		endwhile;

		$arr_infoproduct = [];
		$info_products = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1,) ); 
	    while($info_products->have_posts() ) : 
	        $info_products->the_post();
	        $arr_infoproduct[get_the_title()]['id'] = get_the_ID();
	        $arr_infoproduct[get_the_title()]['option'] = get_field('option', get_the_ID());
	    endwhile;
		$max_discount = get_post_meta($id_code, 'discount', true);
		$max_term = get_post_meta($id_code, 'term', true);
	?>
<?php //the_content(); ?>
<div class="innerCont w1354 kenproducts">
	<div class="largeTitle">
		<h1><?php echo $title; ?></h1>
	</div>
	<div class="box-shadow" style="font-family: Arial, helvetica, sans-serif; margin:0 0px; font-size: 11pt; color: #6c6060;">
		<h2>LifeLock Plan Options</h2>
		<p><strong>LifeLock Individual Plans: </strong><br><strong>Identity Theft Protection Services </strong><br><strong>We do more to protect your credit and your good name.</strong><br><br><br></p>
		<p style="font-size: 13pt !important;"><span style="font-size: 1.17em; font-family: 'Open Sans', sans-serif;" class="pri-title">Prices of LifeLock Plans Include Promo Code: <a href="<?php echo render_url($id_code, $id_page); ?>" style="corlor: #333;" title="LLC3015"><?php the_field('code',$id_code); ?></a> <?php the_field('promotion',$id_code); ?></span></p>
		<div class="garung_tab_mobile">
			<div class="container">
				<div class="row">
					<!-- <div class="span12"> -->
			            <div id="tab" class="btn-group" data-toggle="buttons-radio">
			              <a class="btn active garung_tab_a" data-num="1" data-toggle="tab">Standard</a>
			              <a class="btn garung_tab_a" data-num="2" data-toggle="tab">Advance</a>
			              <a class="btn garung_tab_a" data-num="3" data-toggle="tab">Ultimate Plus</a>
			              <a class="btn garung_tab_a" data-num="4" data-toggle="tab">Junior</a>
			            </div>
			        <!-- </div> -->
			    </div>
			</div>
		</div>
		<!--Features-->
		<div style="background-color: #fffefe; padding: 0px 10px; width: 20%; height: 92%; float: left; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #6c6060;" class="garung_plan_feature">
			<div class="header-more" style="height: 120px; padding-top: 20px; text-align: center;">
				<h2>Plan Features</h2>
			</div>
			<!-- <div class="body-more" style="height: 270px;">&nbsp;</div> -->
			<div class="footer-more" style="padding-top: 295px;">
				<ul id="feature" class="box-title">
					<?php
	                    $get_checkbox = get_field_object('option');
	                    if( $get_checkbox ): ?>
	                        <?php 
	                        	$i = 0;
	                        	foreach( $get_checkbox['choices'] as $color ): 
	                        		if(($i % 2) == 0):
	                        ?>
	                            		<li style="background-color: #f0ebeb; padding: 5px 5px; height: 30px;"><?php echo $color; ?></li>
	                        <?php 
	                        		else:
	                        			if ($i == 9)
	                        ?>
	                    				<li style="padding: 5px 5px; height: 30px;"><?php echo $color; ?></li>
	                    	<?php
	                    			endif;
	                    			$i++;
	                        	endforeach; 
	                    endif;
                    ?>
				</ul>
			</div>
		</div>
		<?php 
			$i = 1;
			$loop = new WP_Query( array( 'post_type' => 'info_products','posts_per_page'=> -1,'orderby'   => 'id','order' => 'ASC',) ); 
			while($loop->have_posts() ) : $loop->the_post();
				$price_month = (float)get_field('price_month', get_the_ID());
				$price_annual = (float)get_field('price_annual', get_the_ID());

				$month = round(($price_month * ((100-$max_discount)/100)), 2);
				$annual = round(($price_annual * ((100-$max_discount)/100)), 2);
				if($i == 1) {
					$background = '#fcfafa';
				} elseif ($i == 2) {
					$background = '#f4f2f2';
				} elseif($i == 3) {
					$background = '#e5e1e1';
				} else {
					$background = '#dcd9d9';
				}
			?>
				<div class="site-product-box-tick garung-product-box column-<?php echo $i; ?>" style="background-color: <?php echo $background; ?>; width: 20%; height: 92%; float: left; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #6c6060; padding: 0px 10px;">
					<div class="header-more" style="height: 130px; padding-top: 20px; text-align: center;">
						<div class="logo-lifelock-text">
							<div class="logo"><img src="<?php bloginfo('template_directory'); ?>/image/logo-icon-text.gif" alt="Life Lock promo code" width="37" height="36"></div>
							<div class="text">
								<p>Life<span>Lock</span></p>
								<p><?php the_title() ?> ™</p>
							</div>
						</div>
						<p style="padding-top: 10px;"><span style="color: #000000;">$<?php echo $month; ?>/month</span></p>
						<p><span style="color: #999;">$<?php echo $annual;  ?>/annual</span></p>
						<p><span style="color: #999;"><?php echo $max_term; ?> DAYS RISK FREE*</span><span style="font-size: 9pt;">&nbsp;</span></p>
						<p>&nbsp;</p>
						<p><a href="<?php echo render_url($id_code, $id_page); ?>" class="btn" style="color: white;" title="Enroll now">Enroll now</a></p>
					</div>
					<div class="body-more" style="height: 270px;">
						<div class="body-more1" style="height: 85px;"></div>
						<div class="body-more2">
							<?php the_content() ?>
						</div>
					</div>
					<div class="footer-more" style="padding: 14px 5px;">
						<ul id="feature" class="detail-tick">
							<?php
                            $checked = get_field('option', get_the_ID());
                            if( $checked ): ?>
                                	<?php 
                                	$j = 0;
                                	foreach( $checked as $key => $check ): 
                                		if(((int)$key%2) == 0):
                                	?>
                                    	<li style="height: 30px; padding: 5px; background-color: #f0ebeb;"><span>&nbsp;</span></li>
		                            <?php 
	                                	else: 
	                                		if($j == 9):
	                                ?>
	                            		<li style="padding: 5px; height: 30px;  line-height: 30px; font-size: 15px; font-weight: bold; text-align: center;" class="check_li_num"><?php echo ((get_field('stolen_funds_replacement', get_the_ID()) != '') ? '$ '.get_field('stolen_funds_replacement', get_the_ID()): ''); ?></li>
	                            	<?php
	                                		else:
		                            ?>
                               			<li style="padding: 5px; height: 30px;"><span></span></li>
	                               	<?php
	                               			endif;
	                                	endif;	                                	
	                               		$j++;
	                                endforeach; ?>
	                                <?php 
	                                // for instance Standard column
                       				if (count($checked) == 9):
                       					?>
                       					<li style="padding: 5px; height: 30px;  line-height: 30px; font-size: 15px; font-weight: bold; text-align: center;" class="check_li_num"><?php echo ((get_field('stolen_funds_replacement', get_the_ID()) != '') ? '$ '.get_field('stolen_funds_replacement', get_the_ID()): ''); ?></li>
                       					<?php
                       				endif;
                       				?>
                            <?php endif; ?>
						</ul>
					</div>
				</div>
			<?php
				$i++;
			endwhile;
		?>
		</div>
		</div><!-- #content -->
	</div><!-- #primary -->
	<div class="box-shadow footer-bot-product" style="font-family: Arial, helvetica, sans-serif; margin:0 0px; font-size: 12px; color: #6c6060;">
		<?php if ( is_active_sidebar( 'footer-bot-product' ) ) : ?>
		    <ul id="sidebar">
		        <?php dynamic_sidebar( 'footer-bot-product' ); ?>
		    </ul>
		<?php endif; ?>
	</div>
<?php endwhile; // end of the loop. ?>
<script type="text/javascript">
	// jQuery(document).ready(function(){
	var window_width = jQuery (window).width();
	if(window_width < 480) {
		jQuery('.column-1').addClass('col_show');
		jQuery('.column-2').hide();
		jQuery('.column-3').hide();
		jQuery('.column-4').hide();

		jQuery('.garung_plan_feature').css('width', '60%');
		jQuery('.garung_tab_a').click(function(){
			var num = jQuery(this).attr('data-num');
			if (num == 1) {
				jQuery('.column-1').addClass('col_show').show();
				jQuery('.column-2').hide();
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
			} 
			if (num == 2) {
				jQuery('.column-2').addClass('col_show').show();
				jQuery('.column-1').hide();
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
			} 
			if (num == 3) {
				jQuery('.column-3').addClass('col_show').show();
				jQuery('.column-1').hide();
				jQuery('.column-2').hide();
				jQuery('.column-4').hide();
			} 
			if (num == 4) {
				jQuery('.column-4').addClass('col_show').show();
				jQuery('.column-1').hide();
				jQuery('.column-2').hide();
				jQuery('.column-3').hide();
			}
		});
	}
	// });
	function responsive_column (col_show) {
		switch (col_show) {
			case 1 : 
				jQuery('.column-1').addClass('col_show');
				jQuery('.column-2').hide();
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
				break;
			case 2: 
				jQuery('.column-2').addClass('col_show');
				jQuery('.column-1').hide();
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
				break;
			case 3: 
				jQuery('.column-3').addClass('col_show');
				jQuery('.column-1').hide();
				jQuery('.column-2').hide();
				jQuery('.column-4').hide();
				break;
			case 4: 
				jQuery('.column-4').addClass('col_show');
				jQuery('.column-1').hide();
				jQuery('.column-2').hide();
				jQuery('.column-3').hide();
				break;
			default : {
				jQuery('.column-1').addClass('col_show');
				jQuery('.column-2').addClass('tab_content_hide');
				jQuery('.column-3').hide();
				jQuery('.column-4').hide();
			}
		}
	}
</script>
<?php get_footer() ?>