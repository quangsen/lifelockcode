<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lifelockcode');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MOw[39KqqHJNdE|I*-W|T_ulGp#59>{w2AzKH>hL(24) ANU-FY+~_s(WOT`A[4F');
define('SECURE_AUTH_KEY',  '$>T!r5eLhyx|sCWVk>CM@U}+H@qWHbPE(o:+_z_vF.<k<L~mb4zUKL:^EP9&M]#y');
define('LOGGED_IN_KEY',    'id>nA}36ln2Q![-7l9E)MIyysGGHG9[mGNqfZOmnpXPd]tVQ/8ute_zg>=vIu!Ke');
define('NONCE_KEY',        'c4]fb=gs=2$SX.~e7u-Is5KU7,yA8YLp&W[5y06QgTw#N7GOku BYT60sXleMr1a');
define('AUTH_SALT',        ':#4EaPqqC`-Yh[=pT{*o*JU:m%Xx$A8Jqc[R4qWB1,m$n*$)UsIIRjC-*&eeYx #');
define('SECURE_AUTH_SALT', 'ePDNc+D7>mHh$*ln*I6?]8Y}5(v!wCAZ|zL)xb7So9dzr8)j^=nrvwf6YHDQe5;?');
define('LOGGED_IN_SALT',   '.{$wJv:aDo@<.uo;a%8SR?z h`y*u~y+8=?:rLZZ7^+7.?~JNNW]EGNS!H5zz8q1');
define('NONCE_SALT',       '98eUBJ x,!.Y#}]/?w(AnfyNn:G^%cDyIBD-rwRwKR&(+@M11*8BJ|2D|&`.]D*o');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
